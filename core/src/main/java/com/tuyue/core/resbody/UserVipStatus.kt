package com.tuyue.core.resbody


/**
 * Create by guojian on 2021/8/12
 * Describe：
 **/
class UserVipStatus (
    val code:Int,
    //会员有效期
    val expiredAt: String,
    //会员有效期
    val isPremiumMember:Boolean,
    val message: String
        )