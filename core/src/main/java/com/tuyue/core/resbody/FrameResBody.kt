package com.tuyue.core.resbody


/**
 * Create by guojian on 2021/3/1
 * Describe：
 **/
data class FrameResBody (
    val frameID: Int,
    //默认def 动态平铺tiled
    val frameType: String = "def",
    val frameTitle: String,
    val frameIcon: String,
    val frameZip: String,
    val frameZipMd5: String,
    val frameUpdateTime: Long,
    val vipResources: Int
    )