package com.tuyue.core.resbody


/**
 * Create by guojian on 2021/8/7
 * Describe：
 **/
class VipResModel (
    val apple_product_id: String,
    val created_at: String,
    val days: Int,
    val description: String,
    val end_time: String,
    val id: Int,
    val months: Int,
    val original_price: Int,
    val platform: Int,
    val price: Int,
    val sort: Int,
    val start_time: String,
    val status: Int,
    val subject: String,
    val updated_at: String
        )