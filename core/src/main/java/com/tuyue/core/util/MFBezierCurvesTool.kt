package com.tuyue.core.util

import android.graphics.Point
import kotlin.math.*


/**
 * Create by guojian on 2021/7/12
 * Describe：
 *【注意】
 *
 * 以下通过固定长度 length，推导出 t 的过程，可以参考这个链接：
 * https://www.zhihu.com/question/27715729/answer/293563315

 * 以下有几个变量需要注意，它们是计算过程中产生的中间变量，为了简化表达式。
 * 假如贝塞尔曲线的起始点是 P0，控制点是 P1，终止点是 P2。
 * 则：

 * float ax = P0.x - 2 * P1.x + P2.x;
 * float ay = P0.y - 2 * P1.y + P2.y;
 * float bx = 2 * P1.x - 2 * P0.x;
 * float by = 2 * P1.y - 2 * P0.y;

 * float A = 4 * (ax * ax + ay * ay);
 * float B = 4 * (ax * bx + ay * by);
 * float C = bx * bx + by * by;
 */
object MFBezierCurvesTool {

    fun pointsWithFrom(from: Point, to: Point, control: Point, pointSize: Float): MutableList<Point> {
        val p0 = from
        val p1 = if (isCenter(control, from, to)) from else control
        val p2 = to

        val ax = p0.x - 2 * p1.x + p2.x
        val ay = p0.y - 2 * p1.y + p2.y
        val bx = 2 * p1.x - 2 * p0.x
        val by = 2 * p1.y - 2 * p0.y

        val A = (4 * (ax * ax + ay * ay)).toFloat()
        val B = (4 * (ax * bx + ay * by)).toFloat()
        val C = (bx * bx + by * by).toFloat()

        val totalLength = lengthWithT(1f, A, B, C)
        val pointsPerLength = 20.0 / pointSize
        val count = max(1, ceil(pointsPerLength * totalLength).toInt())
        val mutArr = mutableListOf<Point>()
        for (i in 0 until count) {
            var t = i * 1.0f / count
            val length = t * totalLength
            t = tWithT(t, length, A, B, C)
            // 根据 t 求出坐标
            val x = (1 - t) * (1 - t) * p0.x + 2 * (1 - t) * t * p1.x + t * t * p2.x
            val y = (1 - t) * (1 - t) * p0.y + 2 * (1 - t) * t * p1.y + t * t * p2.y
            mutArr.add(Point(x.toInt(), y.toInt()))
        }
        return mutArr
    }

    /**
     * 长度函数反函数，根据 length，求出对应的 t，使用牛顿切线法求解
     * @param t 给出的近似的 t，比如求长度占弧长 0.3 的 t，t 应该是接近 0.3，则传入近似值 0.3
     * @param length 目标弧长，实际长度，非占比
     * @param A 见【注意】
     * @param B 见【注意】
     * @param C 见【注意】
     */
    private fun tWithT(t: Float, length: Float, A: Float, B: Float, C: Float): Float {
        var t1 = t
        var t2 = 0f
        while (true) {
            val speed = speedWithT(t1, A, B, C)
            if (speed < 0.0001f) {
                t2 = t1
                break
            }
            val speedWithT = lengthWithT(t1, A, B, C)
            t2 = t1 - (speedWithT - length) / speed
            if (abs(t1 - t2) < 0.0001f) {
                break
            }
            t1 = t2
        }
        return t2
    }

    /**
     * 速度函数 s(t) = sqrt(A * t^2 + B * t + C)
     */
    private fun speedWithT(t: Float, A: Float, B: Float, C: Float): Float {
        val pow = t.toDouble().pow(2.0)
        val max = max((A * pow + B * t + C).toInt(), 0)
        return sqrt(max.toDouble()).toFloat()
    }

    private fun lengthWithT(t: Float, A: Float, B: Float, C: Float): Float {
        if (A < 0.00001f) {
            return 0.0f
        }
        val e = 2.718281828459
        val temp1 = sqrt(C + t * (B + A * t))
        val temp2 = (2 * A * t * temp1 + B * (temp1 - sqrt(C)))
        val temp3 = log((abs(B + 2 * sqrt(A) * sqrt(C) + 0.0001f)).toDouble(), e)
        val temp4 = log((abs(B + 2 * A * t + 2 * sqrt(A) * temp1) + 0.0001f).toDouble(), e)
        val temp5 = 2 * sqrt(A) * temp2
        val temp6 = (B * B - 4 * A * C) * (temp3 - temp4)
        val pow = A.toDouble().pow(1.5f.toDouble())
        return ((temp5 + temp6) / (8 * pow)).toFloat()
    }

    private fun isCenter(centerPoint: Point, fromPoint: Point, toPoint: Point): Boolean {
        val isCenterX = abs((fromPoint.x + toPoint.x) / 2 - centerPoint.x) < 0.0001
        val isCenterY = abs((fromPoint.y + toPoint.y) / 2 - centerPoint.y) < 0.0001
        return isCenterX && isCenterY
    }
}