package com.tuyue.common_sdk.helper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.tuyue.common_sdk.R


/**
 * Create by guojian on 2021/7/17
 * Describe：
 **/
class DoodleHeaderHelper(val context: Context, val icon: Int, val title: String) {

    fun create(): View {
        val view = LayoutInflater.from(context).inflate(R.layout.header_doodle, null)
        val image = view.findViewById<ImageView>(R.id.iv_icon)
        val text = view.findViewById<TextView>(R.id.tv_titel)
        image.setImageResource(icon)
        text.text = title
        return view
    }
}