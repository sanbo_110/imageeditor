package com.tuyue.common_sdk.sp

import com.tuyue.common_sdk.sp.impl.native_impl.SharePreferencesImpl
import com.tuyue.common_sdk.tools.BaseControlCenter

/**
 * sp管理器
 * @see ISharedPreferences 获取实例对象
 * 使用方法，以获取一个常规sp的为例.
 * ```
 * SPManager.Companion.get(SPManager.COMMON)
 * ```
 */
object SPManager {

    const val ASSETS = "assets"
    const val USER = "user"

    fun get(name: String): SharePreferencesImpl {
        return SharePreferencesImpl(name, BaseControlCenter.getContext())
    }
}