package com.tuyue.common_sdk.sp

interface IEditor {

    /**
     * 添加一个String类型的缓存
     * @param key 键
     * @param value 值
     * @return this
     */
    fun putString(key: String, value: String): IEditor

    /**
     * 添加一个Int类型的缓存
     * @param key 键
     * @param value 值
     */
    fun putInt(key: String, value: Int): IEditor

    /**
     * 添加一个Long类型的缓存
     * @param key 键
     * @param value 值
     */
    fun putLong(key: String, value: Long): IEditor

    /**
     * 添加一个布尔类型的缓存
     * @param key 键
     * @param value 值
     */
    fun putBoolean(key: String, value: Boolean): IEditor

    /**
     * 添加一个浮点型的缓存
     * * @param key 键
     * @param value 值
     *
     */
    fun putFloat(key: String, value: Float): IEditor


//    /**
//     * 添加一个集合缓存
//     */
//    fun putDataList(key: String, value: ArrayList<>):IEditor

    /**
     * 移除一个key
     * @param key 键
     */
    fun remove(key: String): IEditor

    /**
     * 清空
     */
    fun clear(): IEditor

    /**
     * 提交
     * @return return true if save sd card success
     */

    fun commit(): Boolean

    /**
     * 提交
     */
    fun apply()



}