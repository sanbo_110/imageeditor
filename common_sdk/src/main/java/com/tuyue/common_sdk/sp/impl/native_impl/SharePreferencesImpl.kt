package com.tuyue.common_sdk.sp.impl.native_impl

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.tuyue.common_sdk.sp.IEditor
import com.tuyue.common_sdk.sp.ISharedPreferences

class SharePreferencesImpl constructor(name: String, context: Context) : ISharedPreferences {

    private var mSharedPreferences: SharedPreferences

    init {
        mSharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    @SuppressLint("CommitPrefEdits")
    override fun editor(): IEditor {
        return SharePreferencesEditorImpl(mSharedPreferences)
    }

    override fun getString(key: String, def: String): String? {
        return mSharedPreferences.getString(key, def)
    }

    override fun getInt(key: String, def: Int): Int {
        return mSharedPreferences.getInt(key, def)
    }

    override fun getLong(key: String, def: Long): Long {
        return mSharedPreferences.getLong(key, def)
    }

    override fun getBoolean(key: String, def: Boolean): Boolean {
        return mSharedPreferences.getBoolean(key, def)
    }

    override fun getFloat(key: String, def: Float): Float {
        return mSharedPreferences.getFloat(key, def)
    }


    override fun getAll(): Map<String, *> {
        return mSharedPreferences.all
    }


}