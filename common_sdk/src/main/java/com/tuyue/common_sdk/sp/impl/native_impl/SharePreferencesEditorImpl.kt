package com.tuyue.common_sdk.sp.impl.native_impl

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.tuyue.common_sdk.sp.IEditor

class SharePreferencesEditorImpl constructor(sharedPreferences: SharedPreferences) : IEditor{
    var mEditor: SharedPreferences.Editor

    init {
        @SuppressLint("CommitPrefEdits")
        mEditor = sharedPreferences.edit()
    }

    override fun putString(key: String, value: String): IEditor {
        mEditor.putString(key, value)
        return this
    }

    override fun putInt(key: String, value: Int): IEditor {
        mEditor.putInt(key, value)
        return this
    }

    override fun putLong(key: String, value: Long): IEditor {
        mEditor.putLong(key, value)
        return this
    }

    override fun putBoolean(key: String, value: Boolean): IEditor {
        mEditor.putBoolean(key, value)
        return this
    }

    override fun putFloat(key: String, value: Float): IEditor {
        mEditor.putFloat(key, value)
        return this
    }

    override fun remove(key: String): IEditor {
        mEditor.remove(key)
        return this
    }

    override fun clear(): IEditor {
        mEditor.clear()
        return this
    }

    override fun commit(): Boolean {
        return mEditor.commit()
    }

    override fun apply() {
        mEditor.apply()
    }


}