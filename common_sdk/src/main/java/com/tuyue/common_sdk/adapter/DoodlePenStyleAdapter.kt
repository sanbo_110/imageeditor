package com.tuyue.common_sdk.adapter

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.activity.ImagePetternActivity
import com.tuyue.common_sdk.model.*


/**
 * Create by guojian on 2021/7/17
 * Describe：
 **/
class DoodlePenStyleAdapter :
    BaseQuickAdapter<PenStyleModel, BaseViewHolder>(R.layout.item_doodle_pen_style) {

    private var mLastItemView: View? = null
    private var mLastPosition = 0

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_CONTENT = 1
    }

    override fun setOnItemClick(v: View, position: Int) {
        val model = data[position]
        if(isNeedMemberPay(model)){
            (context as ImagePetternActivity).payVip()
            return
        }

        super.setOnItemClick(v, position)
        if (mLastPosition == position) return
        v.setBackgroundResource(R.drawable.bg_pettern_item_selected)
        mLastItemView?.let {
            it.background = null
        }
        mLastItemView = v
        mLastPosition = position
    }

    /**
     * 需要购买会员
     */
    private fun isNeedMemberPay(data: PenStyleModel): Boolean{
        if(context is ImagePetternActivity){
            (context as ImagePetternActivity).mConfig?.isMember()?.let {
                //不是会员并且是付费资源需要购买会员
                if(!it){
                    return data.vipResources ==1
                }
            }
        }
        return false
    }

    override fun convert(holder: BaseViewHolder, item: PenStyleModel) {
        val image = holder.getView<ImageView>(R.id.iv_pen_style)
        holder.setVisible(R.id.iv_vip_tip, false)
        if (item.assetsStyle == AssetsStyle.NATIVE) {
            item.icon?.let {
                image.setImageResource(it)
            }
            holder.setVisible(R.id.download_item, false)
        } else {
            if(item.vipResources == 1){
                holder.setVisible(R.id.iv_vip_tip, true)
            }
            Glide.with(image).load(item.brushTextureIcon).into(image)
            holder.setVisible(R.id.download_item, item.paths == null)
        }
        if (mLastPosition == holder.adapterPosition) {
            holder.itemView.setBackgroundResource(R.drawable.bg_pettern_item_selected)
            mLastItemView = holder.itemView
        } else {
            holder.itemView.setBackgroundResource(0)
        }
    }

    fun getLastPosition(): Int {
        return mLastPosition
    }
}