package com.tuyue.common_sdk.adapter

import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.activity.PetternModel

/**
 * create by guojian
 * Date: 2020/12/11
 * 模版的适配器
 */
class PetternAdapter : BaseQuickAdapter<PetternModel, BaseViewHolder>(R.layout.item_pettern) {

    private var mLastItemView: View? = null

    override fun convert(holder: BaseViewHolder, item: PetternModel) {

        holder.setText(R.id.tv_pettern_title, item.title)
        item.icon?.let {
            holder.setImageResource(R.id.iv_pettern_icon, it)
        }?:let {
            holder.setImageDrawable(R.id.iv_pettern_icon, item.drawable)
        }
    }

    override fun setOnItemClick(v: View, position: Int) {
        if(mLastItemView != null){
            mLastItemView?.background = null
        }
        v.setBackgroundResource(R.drawable.bg_pettern_item_selected)
        mLastItemView = v
        super.setOnItemClick(v, position)
    }
}