package com.tuyue.common_sdk.adapter

import android.graphics.Paint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.model.VipPriceModel


/**
 * Create by guojian on 2021/8/2
 * Describe：
 **/
class VipTypeAdapter: BaseMultiItemQuickAdapter<VipPriceModel, BaseViewHolder>() {

    private var mLastView: View? = null
    private var mLastPosition = 1

    companion object{
        const val VIP_HEADER = 0
        const val VIP_CONTENT = 1
    }

    override fun setOnItemClick(v: View, position: Int) {
        super.setOnItemClick(v, position)
        if(position !=0) {
            if(mLastPosition == position)return
            v.setBackgroundResource(R.drawable.vip_type_select)
            val selectImage = v.findViewById<ImageView>(R.id.iv_select_tip)
            selectImage?.setBackgroundResource(R.drawable.icon_vip_select_tip)

            mLastView?.background = null
            mLastView?.findViewById<ImageView>(R.id.iv_select_tip)?.background = null
            mLastView = v
            mLastPosition = position
        }
    }

    init {
        addItemType(VIP_HEADER, R.layout.item_vip_header)
        addItemType(VIP_CONTENT, R.layout.adapter_vip_type)
    }

    override fun convert(holder: BaseViewHolder, item: VipPriceModel) {
        when(item.itemType){
            VIP_HEADER -> {
                holder.setText(R.id.tv_title, item.title)
            }
            VIP_CONTENT -> {
                val tv = holder.getView<TextView>(R.id.tv_price)
                tv.paint.flags = Paint.STRIKE_THRU_TEXT_FLAG
                holder.setText(R.id.tv_title, item.data?.subject)
                holder.setText(R.id.tv_vip_detail, item.data?.description)
                holder.setText(R.id.tv_price, item.data?.original_price.toString())
                holder.setText(R.id.tv_pay, item.data?.price.toString())

                if(holder.adapterPosition == mLastPosition){
                    holder.itemView.setBackgroundResource(R.drawable.vip_type_select)
                    holder.itemView.findViewById<ImageView>(R.id.iv_select_tip).setBackgroundResource(R.drawable.icon_vip_select_tip)
                    mLastView = holder.itemView
                }else{
                    holder.itemView.background = null
                    holder.itemView.findViewById<ImageView>(R.id.iv_select_tip).background = null
                }
            }
        }
    }
}