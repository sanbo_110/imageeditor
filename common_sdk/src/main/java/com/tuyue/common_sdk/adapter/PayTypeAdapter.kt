package com.tuyue.common_sdk.adapter

import android.graphics.Paint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.model.PayPriceModel
import com.tuyue.common_sdk.model.VipPriceModel


/**
 * Create by guojian on 2021/8/2
 * Describe：
 **/
class PayTypeAdapter: BaseMultiItemQuickAdapter<PayPriceModel, BaseViewHolder>() {

    private var mLastView: View? = null
    private var mLastPosition = 1

    companion object{
        const val PAY_HEADER = 0
        const val PAY_CONTENT = 1
    }

    init {
        addItemType(PAY_HEADER, R.layout.item_vip_header)
        addItemType(PAY_CONTENT, R.layout.adapter_pay_type)
    }

    override fun setOnItemClick(v: View, position: Int) {
        super.setOnItemClick(v, position)
        if(position !=0) {
            if(mLastPosition == position)return
            val selectImage = v.findViewById<ImageView>(R.id.iv_select)
            selectImage?.isSelected = true

            mLastView?.findViewById<ImageView>(R.id.iv_select)?.isSelected = false
            mLastView = v
            mLastPosition = position
        }
    }

    override fun convert(holder: BaseViewHolder, item: PayPriceModel) {
        when(item.itemType){
            PAY_HEADER -> {
                holder.setText(R.id.tv_title, item.title)
            }
            PAY_CONTENT -> {
                holder.setText(R.id.tv_title, item.title)
                holder.setImageResource(R.id.iv_icon, item.icon)

                val selectImage = holder.itemView.findViewById<ImageView>(R.id.iv_select)
                if(holder.adapterPosition == mLastPosition){
                    mLastView = holder.itemView
                    selectImage?.isSelected = true
                }else{
                    selectImage?.isSelected = false
                }
            }
        }
    }
}