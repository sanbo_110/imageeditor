
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.tuyue.common_sdk.tools.BaseControlCenter

/**
 * create by guojian
 * Date: 2020/12/7
 */
abstract class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        BaseControlCenter.init(this.application, false)
        super.onCreate(savedInstanceState)
    }

    open fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
//            window
//                .addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window.statusBarColor = color
            window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

            window.setStatusBarColor(Color.parseColor("#f5f5f5"))
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }

    }

}