package com.tuyue.common_sdk.activity

import BaseActivity
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.gyf.immersionbar.ImmersionBar
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.helper.SpannableHelper
import com.tuyue.common_sdk.model.EVENT_REFRESH_PAY_RESULT
import com.tuyue.common_sdk.model.MessageEvent
import com.tuyue.common_sdk.sp.SPManager
import com.tuyue.common_sdk.tools.DensityUtil
import com.tuyue.common_sdk.viewmodel.VipViewModel
import com.tuyue.core.resbody.OrderResultResBody
import kotlinx.android.synthetic.main.activity_pay_result.*
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus


/**
 * Create by guojian on 2021/8/4
 * Describe：
 **/
class PayResultActivity: BaseActivity() {

    private var orderId : String? = null
    private var tradeNo: String? = null
    private var mToken: String? = null
    private var userId: String? = null
    private lateinit var netViewModel: VipViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        immersionBar {
//            statusBarColor(R.color.pay_bar_color)
//            navigationBarColor(R.color.pay_bar_color)
//                statusBarDarkFont(true)
//                navigationBarDarkIcon(true)
//        }
//        window.getDecorView()
//            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
//        window.statusBarColor = ActivityCompat.getColor(this, R.color.pay_bar_color)
//        ImmersionBar.with(this)
////            .statusBarColor(R.color.pay_bar_color)
//            .transparentStatusBar()
//            .statusBarDarkFont(true)
//            .init()
        changeStatusBarColor()

        setContentView(R.layout.activity_pay_result)

        orderId = intent.getStringExtra("orderId")
        tradeNo = intent.getStringExtra("tradeNo")
        userId = intent.getStringExtra("userId")?:SPManager.get(SPManager.USER).getString(
            "userId",
            ""
        )
        mToken = intent.getStringExtra("token")?:SPManager.get(SPManager.USER).getString(
            "token",
            ""
        )
        view_pay.visibility = View.GONE
        view_unpay.visibility = View.VISIBLE

        iv_back.setOnClickListener {
            finish()
        }

        GlobalScope.launch {
            withContext(Dispatchers.Main){
                var count = 4
                while (count >0 && !isSuccess) {
                    count--
                    delay(1000)
                    loadOrderDetail()
                }
                if(!isSuccess){
                    payError()
                }
            }
        }

    }

    private fun payError() {
        view_pay.visibility = View.VISIBLE
        view_unpay.visibility = View.GONE

        iv_icon.setImageResource(R.drawable.icon_pay_error)
        tv_pay_state.text = "支付结果待确认"
        tv_pay_count.visibility = View.GONE
        tv_pay_time.text = "系统还未确认支付结果，"
        tv_tip.text = "请稍后在设置里查看您的会员信息更新状态。"
    }


    var isSuccess = false

    private fun loadOrderDetail() {
        netViewModel = ViewModelProvider(this).get(VipViewModel::class.java)
        val fail = MutableLiveData<OrderResultResBody>()
        netViewModel.getOrderResult(userId, mToken, orderId, tradeNo, fail)
        netViewModel.mOrderResult.observe(this, {
            isSuccess = true
            onResult(it)
        })

    }

    fun onResult(orderResultResBody: OrderResultResBody) {
        view_pay.visibility = View.VISIBLE
        view_unpay.visibility = View.GONE

        val spnn = SpannableHelper.Builder().text("支付金额：").size(DensityUtil.sp2px(18f))
            .text("¥${orderResultResBody.info.amount}").size(DensityUtil.sp2px(26f))
            .text("元").size(DensityUtil.sp2px(18f)).build()

        tv_pay_count.text = spnn
        tv_pay_time.text = "您的会员有效期至：${orderResultResBody.info.expiredAt}"

        EventBus.getDefault().post(MessageEvent(EVENT_REFRESH_PAY_RESULT))
    }

}