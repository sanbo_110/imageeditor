package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.tuyue.common_sdk.tools.DensityUtil


/**
 * Create by guojian on 2021/5/18
 * Describe：
 **/
class DoodleAdjuster(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val mPaint = Paint()
    private var mPath = Path()
    private val mOffset = DensityUtil.dp2Px(20f).toFloat()

    init {
        mPaint.apply {
            color = Color.WHITE
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            isAntiAlias = true
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        mPath  = Path()
        mPath.moveTo(0f, height.toFloat() - mOffset)
        canvas?.let {
            mPath.quadTo(width.div(12).toFloat(), mOffset, width.div(4).toFloat(), mOffset)

            mPath.quadTo(width.times(5).div(12).toFloat(),
                mOffset, width.div(2).toFloat(), height.div(2).toFloat())

            mPath.quadTo(width.times(7).div(12).toFloat(), height.toFloat() - mOffset,
                width.div(4).times(3).toFloat(), height.toFloat() - mOffset)

            mPath.quadTo(width.times(11).div(12).toFloat(),
                height.toFloat() - mOffset, width.toFloat(), mOffset)
            it.drawPath(mPath, mPaint)
        }
    }

    fun adjust(strokeWidth: Float, maskFilter: MaskFilter?){
        mPaint.apply {
            this.strokeWidth = strokeWidth
            this.maskFilter = maskFilter
        }
        invalidate()
    }
}