package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.divyanshu.draw.widget.DrawView
import com.divyanshu.draw.widget.MyPath
import com.divyanshu.draw.widget.PaintOptions
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.activity.PetternModel
import com.tuyue.common_sdk.adapter.PetternAdapter
import com.tuyue.common_sdk.image_edit.PetternType
import com.tuyue.common_sdk.image_edit.PetternType.DOODLE_MASK
import com.tuyue.common_sdk.item_decoration.LinearListItemDecoration
import com.tuyue.common_sdk.tools.DensityUtil
import kotlinx.android.synthetic.main.view_doodle_controler.view.*
import kotlinx.android.synthetic.main.view_draw_layout.view.*
import kotlinx.android.synthetic.main.view_pettern_controler.view.rv_controler
import java.util.*


class DoodleControler(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private val mNormalAdapter = PetternAdapter()
    private var mPenData: MutableList<PetternModel> = mutableListOf()
    private var mEraserData: MutableList<PetternModel> = mutableListOf()
    private lateinit var mDoodleLayout: DrawLayout
    private lateinit var mDoodleAdjuster: DoodleAdjuster
    private var mDoodleType = DoodleType.NARMAL
    private val mProgressQueue: HashMap<String, Int> = hashMapOf()

    //确认笔刷操作后保存笔刷序列
    private var mDrawingList: MutableList<PaletteView.DrawingInfo>? = null

    init {
        inflate(context, R.layout.view_doodle_controler, this)
        initBase()
        setEvent()
    }

    private fun setEvent() {
        iv_doodle_close.setOnClickListener {
            doodle_color_selector.visibility = View.GONE
            doodle_bottom.visibility = View.INVISIBLE
        }
        iv_clear.setOnClickListener {
            mDoodleLayout.draw_view.clear()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
        }
        bg_doodle_undo.setOnClickListener {
            mDoodleLayout.draw_view.undo()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }
        bg_doodle_redo.setOnClickListener {
            mDoodleLayout.draw_view.redo()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }
        mNormalAdapter.setOnItemClickListener { adapter, view, position ->
            if(mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) {
                mDoodleType = when (position) {
                    0 -> DoodleType.COLOR
                    1 -> DoodleType.SIZE
                    2 -> DoodleType.SHADOW
                    else -> DoodleType.NARMAL
                }
                when (mPenData[position].petternType) {
                    PetternType.DOODLE_COLOR -> {
                        doodle_color_selector.visibility = View.VISIBLE
                        doodle_bottom.visibility = View.VISIBLE
                        tone_doodle_seekbar.visibility = View.GONE
                    }
                    PetternType.DOODLE_SIZE -> {
                        tone_doodle_seekbar.visibility = View.VISIBLE
                        tone_doodle_seekbar.progress =
                            mProgressQueue[if (mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) DoodleType.DOODLE_PEN.name else DoodleType.DOODLE_ERASER.name]
                                ?: 0
                    }
                    DOODLE_MASK -> {
                        tone_doodle_seekbar.visibility = View.VISIBLE
                        tone_doodle_seekbar.progress = mProgressQueue[mDoodleType.name] ?: 0
                    }
                }
            }else{
                //橡皮
                mDoodleType = when (position) {
                    0 -> DoodleType.SIZE
                    else -> DoodleType.NARMAL
                }
                when (mEraserData[position].petternType) {
                    PetternType.DOODLE_SIZE -> {
                        tone_doodle_seekbar.visibility = View.VISIBLE
                        tone_doodle_seekbar.progress =
                            mProgressQueue[if (mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) DoodleType.DOODLE_PEN.name else DoodleType.DOODLE_ERASER.name]
                                ?: 0
                    }
                }
            }
        }
        doodle_color_selector.colorSelectCallback(object : ColorSelectCallback {
            override fun onSelect(color: Int) {
                mDoodleLayout.draw_view.penColor = color
                doodle_color_selector.visibility = View.GONE
                doodle_bottom.visibility = View.INVISIBLE
                mNormalAdapter.setData(
                    0,
                    PetternModel(null, "颜色", PetternType.DOODLE_COLOR, buildGradientDrawable(color))
                )
            }
        })
        tone_doodle_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                when (mDoodleType) {
                    DoodleType.SIZE -> {
                        mDoodleLayout.draw_view.setPenRawSize(p1)
                    }
                    DoodleType.SHADOW -> {
                        mDoodleLayout.draw_view.setShadowBlend(p1)
                    }
                }
                mDoodleAdjuster.adjust(
                    if(mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) mDoodleLayout.draw_view.penSize.toFloat() else mDoodleLayout.draw_view.eraserSize.toFloat(),
                    mDoodleLayout.draw_view.mMaskFilter
                )
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                mDoodleAdjuster.visibility = View.VISIBLE
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                mDoodleAdjuster.visibility = View.GONE
                p0?.let {
                    when(mDoodleType){
                        DoodleType.SIZE -> {
                            mProgressQueue[if(mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) DoodleType.DOODLE_PEN.name else DoodleType.DOODLE_ERASER.name] = it.progress
                        }
                        DoodleType.SHADOW -> {
                            mProgressQueue[mDoodleType.name] = it.progress
                        }
                    }
                }
            }
        })

        iv_pen.setOnClickListener {
            mDoodleLayout.draw_view.mode = PaletteView.Mode.DRAW
            iv_pen.isSelected = true
            iv_ereaze.isSelected = false
            tone_doodle_seekbar.progress = mProgressQueue[DoodleType.DOODLE_PEN.name] ?: 0
            mNormalAdapter.setList(mPenData)
        }

        iv_ereaze.setOnClickListener {
            mDoodleLayout.draw_view.mode = PaletteView.Mode.ERASER
            iv_ereaze.isSelected = true
            iv_pen.isSelected = false
            tone_doodle_seekbar.progress = mProgressQueue[DoodleType.DOODLE_ERASER.name] ?: 0
            mNormalAdapter.setList(mEraserData)
        }
    }

    private fun initBase() {
        val manager = LinearLayoutManager(context)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        rv_controler.layoutManager = manager
        rv_controler.adapter = mNormalAdapter
        rv_controler.addItemDecoration(LinearListItemDecoration("#1C1C1C", 1))

        mPenData.add(
            PetternModel(
                null,
                "颜色",
                PetternType.DOODLE_COLOR,
                buildGradientDrawable(Color.WHITE)
            )
        )
        mPenData.add(PetternModel(R.drawable.ic_dx, "大小", PetternType.DOODLE_SIZE))
        mPenData.add(PetternModel(R.drawable.ic_yd, "硬度", DOODLE_MASK))
        mNormalAdapter.setList(mPenData)

        mEraserData.add(PetternModel(R.drawable.ic_dx, "大小", PetternType.DOODLE_SIZE))

        doodle_color_selector.visibility = View.GONE
        doodle_bottom.visibility = View.INVISIBLE
        iv_pen.isSelected = true
    }

    fun init() {
        this.visibility = View.VISIBLE
        mDoodleLayout.draw_view.initDrawList(mDrawingList)
        Log.e("mDrawingList", "")
    }

    private fun buildGradientDrawable(color: Int): GradientDrawable {
        val drawable = GradientDrawable()
        drawable.cornerRadius = DensityUtil.dp2Px(11f).toFloat()
        drawable.setColor(color)
        drawable.setStroke(DensityUtil.dp2Px(2f), Color.WHITE)
        return drawable
    }

    fun saveDrawPath() {
        if (mDoodleLayout.draw_view.mDrawingList == null) return
        mDrawingList?.clear()
        mDrawingList = mutableListOf()
        mDoodleLayout.draw_view.mDrawingList?.forEach {
            mDrawingList?.add(it)
        }
        Log.e("mDrawingList", "")
    }

    fun bindDoodleLayout(doodleView: DrawLayout) {
        mDoodleLayout = doodleView
        mDoodleLayout.draw_view.addTouchAction(object : DrawView.TouchAction {
            override fun event_up() {
                iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
                iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
            }
        })
        post {
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }

        //初始化笔刷橡皮宽度大小
        val initPenSize = 10
        mDoodleLayout.draw_view.setPenRawSize(initPenSize)
        mProgressQueue[DoodleType.DOODLE_PEN.name] = initPenSize
        mProgressQueue[DoodleType.DOODLE_ERASER.name] = initPenSize
    }

    fun bindDoodleAdjuster(doodleAdjuster: DoodleAdjuster) {
        mDoodleAdjuster = doodleAdjuster
    }

    fun outputBitmap(): Bitmap? {
        iv_doodle_undo.isEnabled = false
        return mDoodleLayout.draw_view.buildBitmap()
    }

    fun ouputPaths(): LinkedHashMap<MyPath, PaintOptions> {
        iv_doodle_undo.isEnabled = false
        return linkedMapOf()
    }

    enum class DoodleType {
        NARMAL,
        COLOR,
        SIZE,

        DOODLE_PEN,
        DOODLE_ERASER,

        SHADOW
    }

}