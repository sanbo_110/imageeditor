package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Point
import android.util.AttributeSet
import android.widget.FrameLayout
import com.tuyue.common_sdk.R
import kotlinx.android.synthetic.main.view_draw_layout.view.*


/**
 * Create by guojian on 2021/5/5
 * Describe：
 **/
class DrawLayout(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    //底图
    private var mBitmap: Bitmap? = null

    init {
        inflate(context, R.layout.view_draw_layout, this)
    }

    fun setBitmap(bitmap: Bitmap, bounds: Point?){
        mBitmap = bitmap
        post {
            bounds?.let {
                measureBounds(it, 0)
                back_ground.setImageBitmap(bitmap)
            }
        }

    }


    private fun measureBounds(bounds: Point, offset: Int) {
        if (mBitmap == null) return
        var newWidth = 0
        var newHeight = 0
        mBitmap?.let {
            if (it.width.toFloat().div(it.height.toFloat()) > (bounds.x.toFloat() - 2 * offset).div((bounds.y.toFloat() - 2 * offset))) {
                newWidth = bounds.x - 2 * offset
                newHeight = newWidth.times(it.height).div(it.width)
            } else {
                newHeight = bounds.y - 2 * offset
                newWidth = newHeight.times(it.width).div(it.height)
            }

            val params = layoutParams
            params.width = newWidth
            params.height = newHeight
            layoutParams = params


            val measureBounds = Point(newWidth, newHeight)
            draw_view.setBounds(measureBounds)
        }
    }

}