package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.divyanshu.draw.widget.DrawView
import com.google.android.material.tabs.TabLayout
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.activity.ImagePetternActivity
import com.tuyue.common_sdk.adapter.DoodlePenStyleAdapter
import com.tuyue.common_sdk.helper.GPUImageHelper
import com.tuyue.common_sdk.helper.PetternAssetsHelper
import com.tuyue.common_sdk.item_decoration.LinearListItemDecoration
import com.tuyue.common_sdk.model.AssetsStyle
import com.tuyue.common_sdk.model.PenStyle
import com.tuyue.common_sdk.model.PenStyleModel
import com.tuyue.common_sdk.sp.SPManager
import com.tuyue.core.util.CheckUtils
import com.tuyue.core.viewmodel.PetternViewModel
import kotlinx.android.synthetic.main.view_doodle_controler.view.*
import kotlinx.android.synthetic.main.view_doodle_controler.view.iv_doodle_redo
import kotlinx.android.synthetic.main.view_doodle_controler.view.iv_doodle_undo
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.*
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.iv_pen
import kotlinx.android.synthetic.main.view_draw_layout.view.*
import java.util.*
import kotlinx.android.synthetic.main.view_doodle_controler.view.iv_clear as iv_clear1
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.bg_doodle_redo as bg_doodle_redo1
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.bg_doodle_undo as bg_doodle_undo1
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.doodle_color_selector as doodle_color_selector1
import kotlinx.android.synthetic.main.view_doodle_controler_v2.view.iv_ereaze as iv_ereaze1


/**
 * Create by guojian on 2021/7/16
 * Describe：
 **/
class DoodelControlerV2(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private lateinit var mDoodleLayout: DrawLayout
    private lateinit var mDoodleAdjuster: DoodleAdjuster
    private val mPenstyleAdapter = DoodlePenStyleAdapter()
    private val mCustomStyleAdapter = DoodlePenStyleAdapter()
    private val mNativePenstyleData = mutableListOf<PenStyleModel>()
    private val mNetworkPenstyleData = mutableListOf<PenStyleModel>()
    private val mProgressQueue: HashMap<String, Int> = hashMapOf()
    private lateinit var mPenStyleTabLayout: TabLayout
    private var mSelectPenTab: PenTab = PenTab.BASE

    //确认笔刷操作后保存笔刷序列
    private var mDrawingList: MutableList<MutableList<PaletteView.DrawingInfo>> = mutableListOf()
    private var mRedoDrawingList: MutableList<MutableList<PaletteView.DrawingInfo>> = mutableListOf()

    init {
        inflate(context, R.layout.view_doodle_controler_v2, this)

        initBase()
        initEvent()
    }

    /**
     * 根据position更新笔触
     */
    private fun updateBrushItem(position: Int) {
        mNetworkPenstyleData[position].let { penStyleModel ->
            val list =
                PetternAssetsHelper(context).parseBrushModel(penStyleModel.brushTextureID.toString())
            list?.let { asset ->
                penStyleModel.paths = asset
            }
        }
    }

    private fun selectPenstyle(position: Int){
        val model = mNativePenstyleData[position]
        if (model.penStyle == PenStyle.DRAW) {
            //虚线
            mDoodleLayout.draw_view.isDashPath(true)
            progress_shadow.visibility = View.INVISIBLE
        } else {
            mDoodleLayout.draw_view.isDashPath(false)
            when (model.itemType) {
                DoodlePenStyleAdapter.TYPE_HEADER -> {
                    //无笔刷
                    mDoodleLayout.draw_view.mode = PaletteView.Mode.DRAW
                    progress_shadow.visibility = View.VISIBLE
                }
                else -> {
                    //基础笔刷
                    model.icon?.let {
                        progress_shadow.visibility = View.INVISIBLE
                        mDoodleLayout.draw_view.setPenStyle(it, model.isRotate)
                    }
                }
            }
            mDoodleLayout.draw_view.setPathStyle(model.pathStyle)
        }

    }

    private fun initEvent() {
        //基础笔刷
        mPenstyleAdapter.setOnItemClickListener { adapter, view, position ->
            selectPenstyle(position)
        }
        //素材笔刷
        mCustomStyleAdapter.setOnItemClickListener { adapter, view, position ->
            updateBrushItem(position)
            val customModel = mNetworkPenstyleData[position]
            val localAssetsSaveTime = SPManager.get(SPManager.ASSETS)
                .getLong(customModel.brushTextureID.toString(), 0)
            if (CheckUtils.isEmpty(customModel.paths) || customModel.brushTextureUpdateTime > localAssetsSaveTime) {
                SPManager.get(SPManager.ASSETS).editor().putLong(
                    customModel.brushTextureID.toString(),
                    customModel.brushTextureUpdateTime
                ).commit()
                //手动下载资源
                val loadingView =
                    view.findViewById<DownloadItem>(R.id.download_item) as DownloadItem
                GPUImageHelper.updateBrush(
                    adapter,
                    position,
                    context,
                    customModel,
                    loadingView,
                    object : GPUImageHelper.Observer {
                        override fun just(data: Any) {
                            if (data is PenStyleModel) {
                                //下载后更新笔触
                                data.paths?.let {
                                    mDoodleLayout.draw_view.setPenStyle(it)
                                }
                            }
                        }
                    })
            }else{
                mDoodleLayout.draw_view.setPenStyle(customModel.paths!!)
            }
        }
        iv_pen.setOnClickListener {
            mDoodleLayout.draw_view.mode = PaletteView.Mode.DRAW
            iv_pen.isSelected = true
            iv_ereaze.isSelected = false
            seekbar_size.progress = mProgressQueue[DoodleControler.DoodleType.DOODLE_PEN.name] ?: 0
            group_pen.visibility = View.VISIBLE
            mPenStyleTabLayout.visibility = View.VISIBLE

            when(mSelectPenTab){
                PenTab.BASE -> {
                    selectPenstyle(mPenstyleAdapter.getLastPosition())
                }
                PenTab.CUSTOM -> {
                    if(mNetworkPenstyleData.size > 0) {
                        mNetworkPenstyleData[mCustomStyleAdapter.getLastPosition()].paths?.let {
                            mDoodleLayout.draw_view.setPenStyle(it)
                        }
                    }
                }
            }
        }
        iv_ereaze.setOnClickListener {
            mDoodleLayout.draw_view.mode = PaletteView.Mode.ERASER
            iv_ereaze.isSelected = true
            iv_pen.isSelected = false
            seekbar_size.progress =
                mProgressQueue[DoodleControler.DoodleType.DOODLE_ERASER.name] ?: 0
            group_eraser.visibility = View.INVISIBLE
            mPenStyleTabLayout.visibility = View.INVISIBLE
        }
        iv_clear.setOnClickListener {
            mDoodleLayout.draw_view.clear()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
        }
        bg_doodle_undo.setOnClickListener {
            mDoodleLayout.draw_view.undo()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }
        bg_doodle_redo.setOnClickListener {
            mDoodleLayout.draw_view.redo()
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }
        seekbar_size.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if(mDoodleLayout.draw_view.mode != PaletteView.Mode.PATH_EFFECT) {
                    mDoodleLayout.draw_view.setPenRawSize(p1)
                    mDoodleAdjuster.adjust(
                        if (mDoodleLayout.draw_view.mode == PaletteView.Mode.ERASER) mDoodleLayout.draw_view.eraserSize.toFloat() else mDoodleLayout.draw_view.penSize.toFloat(),
                        mDoodleLayout.draw_view.mMaskFilter
                    )
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                mDoodleAdjuster.visibility = if(mDoodleLayout.draw_view.mode != PaletteView.Mode.PATH_EFFECT) View.VISIBLE else View.GONE
            }

            override fun onStopTrackingTouch(p0: SeekBar) {
                if(mDoodleLayout.draw_view.mode == PaletteView.Mode.PATH_EFFECT){
                    mDoodleLayout.draw_view.setPenRawSize(p0.progress)
                }
                mDoodleAdjuster.visibility = View.GONE
                mProgressQueue[if (mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) DoodleControler.DoodleType.DOODLE_PEN.name else DoodleControler.DoodleType.DOODLE_ERASER.name] =
                    p0.progress
            }
        })
        seekbar_shadow.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                mDoodleLayout.draw_view.setShadowBlend(p1)
                mDoodleAdjuster.adjust(
                    if (mDoodleLayout.draw_view.mode == PaletteView.Mode.DRAW) mDoodleLayout.draw_view.penSize.toFloat() else mDoodleLayout.draw_view.eraserSize.toFloat(),
                    mDoodleLayout.draw_view.mMaskFilter
                )
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                mDoodleAdjuster.visibility = View.VISIBLE
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                mDoodleAdjuster.visibility = View.GONE
                mProgressQueue[DoodleControler.DoodleType.SHADOW.name] = p0!!.progress
            }
        })
        doodle_color_selector.colorSelectCallback(object : ColorSelectCallback {
            override fun onSelect(color: Int) {
                mDoodleLayout.draw_view.penColor = color
            }
        })
    }

    fun saveDrawPath() {
        if (mDoodleLayout.draw_view.mDrawingList == null) return
//        mDrawingList?.clear()
        val list = mutableListOf<PaletteView.DrawingInfo>()
        mDoodleLayout.draw_view.mDrawingList?.forEach {
            list.add(it)
        }
        mDrawingList.add(list)
        Log.e("mDrawingList", "")
    }

    fun undo(){
        if(mDrawingList.size > 0){
            val undoList = mutableListOf<PaletteView.DrawingInfo>()
            mDrawingList.last().forEach {
                undoList.add(it)
            }
            mRedoDrawingList.add(undoList)
            mDrawingList.removeLast()
        }
    }


    fun redo(){
        if(mRedoDrawingList.size > 0){
            val redoList = mutableListOf<PaletteView.DrawingInfo>()
            mRedoDrawingList.last().forEach {
                redoList.add(it)
            }
            mDrawingList.add(redoList)
            mRedoDrawingList.removeLast()
        }
    }

    fun init() {
        this.visibility = View.VISIBLE
        if(mDrawingList.size > 0) {
            mDoodleLayout.draw_view.initDrawList(mDrawingList.last())
        }else{
            mDoodleLayout.draw_view.initDrawList(null)
        }
        iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
        iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        Log.e("mDrawingList", "")
    }

    private fun initBase() {
        val manager = LinearLayoutManager(context)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        rv_pen.layoutManager = manager
        rv_pen.adapter = mPenstyleAdapter
        rv_pen.addItemDecoration(LinearListItemDecoration("#1C1C1C", 1))

        val header = PenStyleModel(DoodlePenStyleAdapter.TYPE_HEADER)
        header.icon = R.drawable.ic_hb0
        mNativePenstyleData.add(header)

        val content2 = PenStyleModel(DoodlePenStyleAdapter.TYPE_CONTENT)
        content2.icon = R.drawable.bifeng
        mNativePenstyleData.add(content2)

        val content3 = PenStyleModel(DoodlePenStyleAdapter.TYPE_CONTENT)
        content3.icon = R.drawable.ic_hb3
        content3.penStyle = PenStyle.DRAW
        mNativePenstyleData.add(content3)

        buildRotateModel()

        mPenstyleAdapter.setList(mNativePenstyleData)

        val customManager = LinearLayoutManager(context)
        customManager.orientation = LinearLayoutManager.HORIZONTAL
        rv_custom_style.layoutManager = customManager
        rv_custom_style.adapter = mCustomStyleAdapter
        rv_custom_style.addItemDecoration(LinearListItemDecoration("#1C1C1C", 1))
        loadBrushtexturelist()

        iv_pen.isSelected = true

    }

    private fun buildRotateModel(){
        val drawableArray = intArrayOf(R.drawable.brush4, R.drawable.ic_hb2, R.drawable.ic_hb3, R.drawable.ic_hb6, R.drawable.ic_hb7,
            R.drawable.ic_hb8, R.drawable.ic_hb9, R.drawable.ic_hb10)

        for (i in 0..drawableArray.lastIndex){
            val content = PenStyleModel(DoodlePenStyleAdapter.TYPE_CONTENT)
            content.icon = drawableArray[i]
            content.pathStyle = PaletteView.PathStyle.CUSTOM
            content.isRotate = true
            mNativePenstyleData.add(content)
        }
    }

    /**
     * 加载素材笔刷数据
     */
    private fun loadBrushtexturelist() {
        val netViewModel = ViewModelProvider(context as ImagePetternActivity).get(PetternViewModel::class.java)
        netViewModel.getBrushList()
        netViewModel.mBrushList.observeForever { list ->
            for(i in 0..list.lastIndex){
                val content = PenStyleModel(DoodlePenStyleAdapter.TYPE_CONTENT)
                content.brushTextureID = list[i].brushTextureID
                content.assetsStyle = AssetsStyle.NETWORK
                content.brushTextureTitle = list[i].brushTextureTitle
                content.brushTextureUpdateTime = list[i].brushTextureUpdateTime
                content.brushTextureZipMd5 = list[i].brushTextureZipMd5
                content.brushTextureIcon = list[i].brushTextureIcon
                content.brushTextureZip = list[i].brushTextureZip
                content.brushTextureType = list[i].brushTextureType
                content.vipResources = list[i].vipResources
                content.showTextureRandom = list[i].showTextureRandom
                mNetworkPenstyleData.add(content)
                updateBrushItem(i)
            }

            mCustomStyleAdapter.setList(mNetworkPenstyleData)
        }


    }

    fun bindDoodleLayout(doodleView: DrawLayout) {
        mDoodleLayout = doodleView
        mDoodleLayout.draw_view.addTouchAction(object : DrawView.TouchAction {
            override fun event_up() {
                iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
                iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
            }
        })
        post {
            iv_doodle_undo.isEnabled = mDoodleLayout.draw_view.canUndo()
            iv_doodle_redo.isEnabled = mDoodleLayout.draw_view.canRedo()
        }

        //初始化笔刷橡皮宽度大小
        val initPenSize = 20
        mDoodleLayout.draw_view.setPenRawSize(initPenSize)
        mProgressQueue[DoodleControler.DoodleType.DOODLE_PEN.name] = initPenSize
        mProgressQueue[DoodleControler.DoodleType.DOODLE_ERASER.name] = initPenSize
    }

    fun bindDoodleAdjuster(doodleAdjuster: DoodleAdjuster) {
        mDoodleAdjuster = doodleAdjuster
    }

    fun bindTabLayout(tabLayout: TabLayout) {
        mPenStyleTabLayout = tabLayout
        mPenStyleTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let { tab1 ->
                    when (tab1.text) {
                        "基础笔刷" -> {
                            mSelectPenTab = PenTab.BASE
                            progress_shadow.visibility = View.VISIBLE
                            doodle_color_selector.visibility = View.VISIBLE
                            rv_pen.visibility = View.VISIBLE
                            rv_custom_style.visibility = View.GONE

                            mDoodleLayout.draw_view.hasColorMatrix(true)
                            selectPenstyle(mPenstyleAdapter.getLastPosition())
                        }
                        else -> {
                            mSelectPenTab = PenTab.CUSTOM
                            progress_shadow.visibility = View.INVISIBLE
                            doodle_color_selector.visibility = View.GONE
                            rv_pen.visibility = View.GONE
                            rv_custom_style.visibility = View.VISIBLE

                            if(mNetworkPenstyleData.size > 0) {
                                mDoodleLayout.draw_view.hasColorMatrix(false)
                                mNetworkPenstyleData[mCustomStyleAdapter.getLastPosition()].paths?.let {
                                    mDoodleLayout.draw_view.setPenStyle(it)
                                }
                            }
                        }
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    fun outputBitmap(): Bitmap? {
        iv_doodle_undo.isEnabled = false
        return mDoodleLayout.draw_view.buildBitmap()
    }

    enum class PenTab{
        BASE, CUSTOM
    }

}