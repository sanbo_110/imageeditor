package com.tuyue.common_sdk.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.adapter.DoodlePenStyleAdapter
import com.tuyue.common_sdk.item_decoration.LinearListItemDecoration
import com.tuyue.common_sdk.model.DoodleColorModel
import kotlinx.android.synthetic.main.view_doodle_selector.view.*


/**
 * class DoodleColorSelector
 **/
class DoodleColorSelector(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    private val mAdapter = Adapter()
    var mColor = Color.WHITE
    private val colors = mutableListOf<DoodleColorModel>()
    private var mCallback: ColorSelectCallback? = null

    init {
        inflate(context, R.layout.view_doodle_selector, this)

        val manager = LinearLayoutManager(context)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        rv_select_color.layoutManager = manager
        rv_select_color.adapter = mAdapter
        rv_select_color.addItemDecoration(LinearListItemDecoration("#1C1C1C", 1))

//        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_HEADER))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT, Color.WHITE))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#7D7D7D")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.BLACK))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#66CCFF")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#6687FF")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#8766FF")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#DE66FF")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#FC66CC")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#E54F4F")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#F28754")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#FECC66")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#FFF763")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#CCFF66")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#54FF87")))
        colors.add(DoodleColorModel(DoodlePenStyleAdapter.TYPE_CONTENT,Color.parseColor("#54FFEB")))
        mAdapter.setNewInstance(colors)

        initEvent()
    }

    private fun initEvent() {
        mAdapter.setOnItemClickListener { adapter, view, position ->
            mColor = colors[position].color
            mCallback?.onSelect(mColor)
        }
    }

    fun colorSelectCallback(callback: ColorSelectCallback){
        mCallback = callback
    }

}
class Adapter() : BaseMultiItemQuickAdapter<DoodleColorModel, BaseViewHolder>() {

    private var mLastItemView: View? = null
    private var mLastPosition = 0

    init {
        addItemType(DoodlePenStyleAdapter.TYPE_HEADER, R.layout.header_doodle)
        addItemType(DoodlePenStyleAdapter.TYPE_CONTENT, R.layout.item_doodle_color_selector)
    }

    override fun setOnItemClick(v: View, position: Int) {
        super.setOnItemClick(v, position)
        if(mLastPosition == position)return
        v.findViewById<ImageView>(R.id.src).setBackgroundResource(R.drawable.bg_pettern_item_selected)
        mLastItemView?.let {
            it.findViewById<ImageView>(R.id.src).background = null
        }
        mLastItemView = v
        mLastPosition = position
    }

    override fun convert(holder: BaseViewHolder, item: DoodleColorModel) {
        if(holder.adapterPosition == mLastPosition){
            holder.itemView.findViewById<ImageView>(R.id.src).setBackgroundResource(R.drawable.bg_pettern_item_selected)
            mLastItemView = holder.itemView
        }else{
            holder.itemView.findViewById<ImageView>(R.id.src).background = null
        }
        when(item.itemType){
            DoodlePenStyleAdapter.TYPE_HEADER ->{
                holder.setImageResource(R.id.iv_icon, R.drawable.ic_xg)
                holder.setText(R.id.tv_titel, "···")
            }
            DoodlePenStyleAdapter.TYPE_CONTENT -> {
                holder.setBackgroundColor(R.id.image, item.color)
            }
        }
    }

}

interface ColorSelectCallback{
    fun onSelect(color: Int)
}