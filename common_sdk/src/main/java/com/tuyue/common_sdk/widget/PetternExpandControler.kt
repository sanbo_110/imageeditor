package com.tuyue.common_sdk.widget

import RandomColorBg
import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseNodeAdapter
import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.luck.picture.lib.tools.ToastUtils
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.activity.ImagePetternActivity
import com.tuyue.common_sdk.activity.PetternModel
import com.tuyue.common_sdk.adapter.PetternAdapter
import com.tuyue.common_sdk.helper.GPUImageHelper
import com.tuyue.common_sdk.helper.PetternAssetsHelper
import com.tuyue.common_sdk.helper.PetternDataHelper
import com.tuyue.common_sdk.helper.PictureSelectorHelper
import com.tuyue.common_sdk.image_edit.GPUImageModel
import com.tuyue.common_sdk.image_edit.PetternType
import com.tuyue.common_sdk.image_edit.UndoRedoHelperImpl_v2
import com.tuyue.common_sdk.image_edit.provider.BaseEditorProvider
import com.tuyue.common_sdk.item_decoration.LinearListItemDecoration
import com.tuyue.common_sdk.model.*
import com.tuyue.common_sdk.sp.SPManager
import com.tuyue.common_sdk.sp.SPManager.ASSETS
import com.tuyue.common_sdk.tflite.StyleTransferModelExecutor
import com.tuyue.common_sdk.tflite.TfLiteImageUtils
import com.tuyue.common_sdk.tflite.model.ModelExecutionResult
import com.tuyue.common_sdk.tools.BitmapUtil
import com.tuyue.common_sdk.tools.DensityUtil
import com.tuyue.common_sdk.tools.GPUImageFilterTools
import com.tuyue.core.resbody.FrameResBody
import com.tuyue.core.resbody.StyleTransferResBody
import com.tuyue.core.resbody.TextureResBody
import com.tuyue.core.util.CheckUtils
import com.tuyue.core.viewmodel.PetternViewModel
import com.xinlan.imageeditlibrary.editimage.utils.BitmapUtils
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.filter.GPUImageAlphaBlendFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import kotlinx.android.synthetic.main.view_pettern_controler.view.*
import kotlinx.coroutines.*


/**
 * create by guojian
 * Date: 2020/12/14
 * 处理模版图像的控制器
 */
class PetternExpandControler(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {

    /**
     * GPU渲染组件
     */
    private var mGpuImageLayout: GPUImageLayout = GPUImageLayout(context)
    private var mFilterAdjuster: GPUImageFilterTools.FilterAdjuster? = null
    private val mUndoRedoHelper: UndoRedoHelperImpl_v2 = UndoRedoHelperImpl_v2()

    /**
     * 功能适配器
     */
    private val mAdapter = PetternExpandAdapter()

    /**
     * 模版适配器
     */
    private val mNormalAdapter = PetternAdapter()
    private var mPetternData = mutableListOf<PetternModel>()

    /**
     * 控制器当前模式
     */
    private var mPetternType = PetternType.NORMAL
    private var mLastPetternType = PetternType.NORMAL

    /**
     * 编辑裁剪一级数据
     */
    private var mCropData: MutableList<CropNode> = mutableListOf()

    /**
     * 滤镜一级数据
     */
    private var mFilterData: MutableList<SecondNode> = mutableListOf()

    /**
     * 调整一级数据
     */
    private var mAdjustData: MutableList<SecondNode> = mutableListOf()

    /**
     * 纹理一级数据
     */
    private var mTextureData: MutableList<SecondNode> = mutableListOf()

    /**
     * 焦点数据
     */
    private var mFocusData: MutableList<SecondNode> = mutableListOf()

    /**
     * 风格迁移一级数据
     */
    private var mStyleTransferData: MutableList<SecondNode> = mutableListOf()

    /**
     * 边框一级数据
     */
    private var mFrameData: MutableList<SecondNode> = mutableListOf()

    /**
     * 裁剪框
     */
    private var mCropImageLayout: CropImageLayout? = null

    /**
     * 边框组件
     */
    private var mImageFrameLayout: ImageFrameLayout? = null

    /**
     * 涂鸦组件
     */
    private var mDoodleLayout: DrawLayout? = null

    /**
     * 原图
     */
    private var mUriPath: String? = null
    private lateinit var mCropResult: Bitmap

    private var mControlerStateLisener: OnContralerStateLisener? = null

    private lateinit var mStyleTransferModelExecutor: StyleTransferModelExecutor

    private var mCurrentGpuimageFilter: GPUImageFilter? = null

    //编辑状态
    private var isEdit = false

    private var mInitPisition: Int = -1

    private var hasProgress = false

    private var mCurrentPosition = 0

    private var showFilterParams: Boolean = false


    init {

        initView()

        initFirstLevelData()
        initCropList()
        initAdjustList()
        initFocusList()

        initEvent()
    }

    private fun initFocusList() {
        mFocusData = PetternDataHelper.initFocusData(context)
    }

    /**
     * 支付会员后刷新
     */
    fun onResultVipPay(){
        mFrameData.forEach {
            if(it is FrameAssetsModel){
                it.vipResources = 1
            }
        }
        mTextureData.forEach {
            if(it is TextureAssetsModel){
                it.vipResources = 1
            }
        }
        mStyleTransferData.forEach {
            if(it is StyleTransferAssetsModel){
                it.vipResources = 1
            }
        }
        when(mPetternType){
            PetternType.FRAME->{
                mAdapter.setNewInstance(mFrameData as MutableList<BaseNode>)
            }
            PetternType.TEXTURE->{
                mAdapter.setNewInstance(mTextureData as MutableList<BaseNode>)
            }
            PetternType.STYLE_TRANSFER->{
                mAdapter.setNewInstance(mStyleTransferData as MutableList<BaseNode>)
            }
            PetternType.DOODLE ->{
//                doodle_controler.onResultVipPay()
            }
        }
    }

    /**
     * 初始化边框数据
     */
    private fun initFrameList() {
        mFrameData.clear()
        mFrameData.add(FrameNode("无边框", null, R.drawable.ic_no_frame, PetternType.FRAME, null))
        val mFrameList = MutableLiveData<List<FrameResBody>>()
        val netViewModel =
            ViewModelProvider(context as ImagePetternActivity).get(PetternViewModel::class.java)
        netViewModel.getFrameList(mFrameList)
        mFrameList.observe(context as ImagePetternActivity, Observer { list ->
            var assets: MutableList<FrameAssetsModel>?
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    list?.forEach {
                        val assetsModel = FrameAssetsModel(PetternType.FRAME)
                        assetsModel.imageUrl = it.frameIcon
                        assetsModel.frameId = it.frameID
                        assetsModel.frameZipMd5 = it.frameZipMd5
                        assetsModel.frameZipUrl = it.frameZip
                        assetsModel.frameUpdateTime = it.frameUpdateTime
                        assetsModel.frameType = it.frameType
                        assetsModel.vipResources = it.vipResources
                        mFrameData.add(assetsModel)
                    }

                    assets = PetternAssetsHelper(context).parseFrameAssets()
                    assets?.let { list ->
                        list.forEach { asset ->
                            mFrameData.forEach {
                                if (it is FrameAssetsModel && asset.left.contains(it.frameId.toString())) {
                                    it.left = asset.left
                                    it.right = asset.right
                                    it.up = asset.up
                                    it.down = asset.down
                                    it.up_left = asset.up_left
                                    it.up_right = asset.up_right
                                    it.down_left = asset.down_left
                                    it.down_right = asset.down_right
                                    it.frameZip = asset.frameZip
                                }
                            }
                        }
                    }

                }
                withContext(Dispatchers.Main) {
                    mAdapter.setNewInstance(mFrameData as MutableList<BaseNode>)
                }
            }
        })
    }

    /**
     * 初始化纹理数据
     */
    private fun initTextureList() {
        mTextureData.clear()
        val customAssets = TextureAssetsModel(PetternType.TEXTURE)
        customAssets.textureTitle = "自定义"
        customAssets.textureIcon = R.drawable.ic_ailj_add
        mTextureData.add(customAssets)
        mTextureData.add(SecondNode("无", null, null, PetternType.TEXTURE))
        val netViewModel =
            ViewModelProvider(context as ImagePetternActivity).get(PetternViewModel::class.java)
        val mTextureList = MutableLiveData<List<TextureResBody>>()
        netViewModel.getTextureList(mTextureList)
        mTextureList.observe(context as ImagePetternActivity, Observer { list ->
            var assets: MutableList<TextureAssetsModel>?
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    list?.forEach {
                        val assetsModel = TextureAssetsModel(PetternType.TEXTURE)
                        assetsModel.textureTitle = it.textureTitle
                        assetsModel.iconUrl = it.textureIcon
                        assetsModel.textureId = it.textureID
                        assetsModel.textureZip = it.textureZip
                        assetsModel.textureZipMd5 = it.textureZipMd5
                        assetsModel.textureUpdateTime = it.textureUpdateTime
                        assetsModel.vipResources = it.vipResources
                        mTextureData.add(assetsModel)
                    }
                    assets = PetternAssetsHelper(context).parseTextureAssets()
                    assets?.let { list ->
                        list.forEach { asset ->
                            mTextureData.forEach {
                                if (it is TextureAssetsModel && asset.path.contains(it.textureId.toString())) {
                                    it.path = asset.path
                                }
                            }
                        }
                    }
                }
                withContext(Dispatchers.Main) {
                    mAdapter.setNewInstance(mTextureData as MutableList<BaseNode>)
                }
            }
        })
    }

    /**
     * 初始化风格迁移数据
     */
    private fun initStyleTransferList() {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                mStyleTransferModelExecutor = StyleTransferModelExecutor(context, false)
                mStyleTransferModelExecutor.buildContentBitmap(mGpuImageLayout.capture())
            }
        }
        mStyleTransferData.clear()
        val customAssets = StyleTransferAssetsModel(PetternType.STYLE_TRANSFER)
        customAssets.styletransferTitle = "自定义"
        customAssets.styleIcon = R.drawable.ic_ailj_add
        mStyleTransferData.add(customAssets)
        val commonAssets = StyleTransferAssetsModel(PetternType.STYLE_TRANSFER)
        commonAssets.styletransferTitle = "无"
        mStyleTransferData.add(commonAssets)
        val netViewModel =
            ViewModelProvider(context as ImagePetternActivity).get(PetternViewModel::class.java)
        val styleTransferList = MutableLiveData<List<StyleTransferResBody>>()
        netViewModel.getStyleTransferList(styleTransferList)
        styleTransferList.observe(context as ImagePetternActivity, Observer { list ->
            var assets: MutableList<StyleTransferAssetsModel>?
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    list?.forEach {
                        val assetsModel = StyleTransferAssetsModel(PetternType.STYLE_TRANSFER)
                        assetsModel.styletransferID = it.styletransferID
                        assetsModel.iconUrl = it.styletransferIcon
                        assetsModel.styletransferZip = it.styletransferZip
                        assetsModel.styletransferTime = it.styletransferTime
                        assetsModel.styletransferTitle = it.styletransferTitle
                        assetsModel.styletransferZipMd5 = it.styletransferZipMd5
                        assetsModel.styletransferBlendingRatio = it.styletransferBlendingRatio
                        assetsModel.vipResources = it.vipResources
                        mStyleTransferData.add(assetsModel)
                    }
                    assets = PetternAssetsHelper(context).parseStyleTransferAssets()
                    for (i in 0..mStyleTransferData.lastIndex) {
                        updateStyleTransferItem(i - 1)
                    }
                    assets?.let { list ->
                        list.forEach { asset ->
                            mStyleTransferData.forEach {
                                if (it is TextureAssetsModel && asset.path.contains(it.textureId.toString())) {
                                    it.path = asset.path
                                }
                            }
                        }
                    }
                }
                withContext(Dispatchers.Main) {
                    mAdapter.setNewInstance(mStyleTransferData as MutableList<BaseNode>)
                }
            }
        })
    }

    /**
     * 调整数据初始化
     */
    private fun initAdjustList() {
        mAdjustData = PetternDataHelper.initAdjustData(context)
    }

    /**
     * 编辑数据初始化
     */
    private fun initCropList() {
        mCropData = PetternDataHelper.initCropData()
    }

    /**
     * 滤镜数据初始化
     */
    private fun initFilterList() {
        mFilterData = PetternDataHelper.initFilterData_v2(context, mUriPath)
    }

    /**
     * 一级node数据
     */
    private fun initFirstLevelData() {
        mPetternData = PetternDataHelper.initFirstLevelData()
        mNormalAdapter.setNewInstance(mPetternData)
    }

    private fun onSelectFirstLevel(position: Int) {
        mPetternType = mPetternData[position].petternType
        tv_center_title.text = mPetternData[position].title
        if(mPetternType == PetternType.DOODLE){
            tv_center_title.visibility = View.GONE
            tab_layout.visibility = View.VISIBLE
        }else{
            tv_center_title.visibility = View.VISIBLE
            tab_layout.visibility = View.GONE
        }

        checkUndoRedoVisible(mPetternType == PetternType.ADJUST)
        checkUpdateButton(true)
        expand_header.visibility =
            if (mPetternType == PetternType.EDIT) View.VISIBLE else View.GONE

        //还原上一次记录选中的位置
        mUndoRedoHelper.getLastPositionByType(mPetternType)?.let { selectRecordModel ->
            mAdapter.setSecondPosition(selectRecordModel.position.plus(1))
            if (selectRecordModel.hasProgress) {
                tone_seekbar.visibility = View.VISIBLE
                val filter = mFilterAdjuster?.filter
                filter?.let { gpuImageFilter ->
                    val progress = mUndoRedoHelper.getLastFilterProgress(gpuImageFilter)?.progress
                        ?: mFilterAdjuster?.defaultPercent
                    val filter = mUndoRedoHelper.getSelectFilter(gpuImageFilter)
                    mFilterAdjuster = GPUImageFilterTools.FilterAdjuster(filter)
                    progress?.let {
                        tone_seekbar.progress = it
                        mFilterAdjuster?.adjust(it)
                    }
                }
            }
        } ?: let {
            mAdapter.setSecondPosition(0)
        }
        mAdapter.notifyDataSetChanged()

        when (mPetternType) {
            PetternType.EDIT -> {
                rv_controler.adapter = mAdapter
                mAdapter.setList(mCropData)
//                val cropBitmap = mGpuImageLayout.capture()
                mCropImageLayout?.setImageBitmap(mCropResult)
                onClickEdit(0)
                mAdapter.setSecondPosition(1)
            }
            PetternType.FILTER -> {
                rv_controler.adapter = mAdapter
                mAdapter.setList(mFilterData)
            }
            PetternType.ADJUST -> {
                rv_controler.adapter = mAdapter
                mAdapter.setList(mAdjustData)
                resetUndoRedoState()
            }
            PetternType.TEXTURE -> {
                initTextureList()
                rv_controler.adapter = mAdapter
                mAdapter.setList(mTextureData)
                mAdapter.setSecondPosition(1)
            }
            PetternType.FRAME -> {
                initFrameList()
                rv_controler.adapter = mAdapter
                mAdapter.setList(mFrameData)
                onClickFrame()
            }
            PetternType.DOODLE -> {
                doodle_controler.init()
                mDoodleLayout?.visibility = View.VISIBLE
                mDoodleLayout?.setBitmap(mGpuImageLayout.capture(), mGpuImageLayout.mInitBounds)
                mGpuImageLayout.visibility = View.GONE
            }
            PetternType.FOCUS -> {
                rv_controler.adapter = mAdapter
                mAdapter.setList(mFocusData)
            }
            PetternType.STYLE_TRANSFER -> {
                initStyleTransferList()
                rv_controler.adapter = mAdapter
                mAdapter.setList(mStyleTransferData)
                mAdapter.setSecondPosition(1)
            }
        }

        mLastPetternType = mPetternType
    }

    private fun initEvent() {
        //选择模式
        mNormalAdapter.setOnItemClickListener { _, _, position ->
            onSelectFirstLevel(position)
        }
        //一级事件处理
        mAdapter.addLevelItemCallback(object : FirstLevelClickCallback {
            override fun onLevelClick(view: View, data: BaseNode, position: Int) {
                when (mPetternType) {
                    PetternType.FRAME -> {
                        if (data is FirstNode) {
                            data.icon?.let {
                                mImageFrameLayout?.setFrame(it)
                            }
                        }
                    }
                }
            }
        })
        //二级Node添加事件
        mAdapter.addNodeItemCallback(object : NodeItemClickCallback {
            override fun onNodeItemClick(
                lastView: View?,
                currentView: View,
                data: BaseNode,
                position: Int,
                extra: Any?
            ) {
                mCurrentPosition = position
                extra?.let {
                    if (it is GPUImageFilter) {
                        onSelectFilter(it, position)
                        mCurrentGpuimageFilter = it
                    }
                } ?: let {
                    tone_seekbar.visibility = View.GONE
                }
                isEdit = true
                when (mPetternType) {
                    PetternType.EDIT -> {
                        if (position < 0) {
                            mUriPath?.let {
                                mCropImageLayout?.setImageBitmap(it)
                                onClickEdit(0)
                            }
                        } else {
                            onClickEdit(position)
                        }
                    }
                    PetternType.FILTER, PetternType.TEXTURE, PetternType.ADJUST -> {
                        //重置滤镜
                        if (position < 0 || (mPetternType == PetternType.TEXTURE && position == 0)) {
                            mUndoRedoHelper.clearFilterByType(mPetternType)
                            renderFilter()
                            resetUndoRedoState()
                        }
                    }
                    PetternType.FRAME -> {
                        mImageFrameLayout?.let { image_frame_view ->
                            mFrameData[position.plus(1)].let {
                                when (it) {
                                    is FrameResNode -> {
                                        image_frame_view.setFrameResouce(it)
                                    }
                                    is FrameAssetsModel -> {
                                        if (CheckUtils.isEmpty(it.left)) {
                                            updateFrameItem(position)
                                            val localAssetsSaveTime = SPManager.get(ASSETS)
                                                .getLong(it.frameId.toString(), 0)
                                            if (CheckUtils.isEmpty(it.left) || it.frameUpdateTime > localAssetsSaveTime) {
                                                SPManager.get(ASSETS).editor().putLong(
                                                    it.frameId.toString(),
                                                    it.frameUpdateTime
                                                ).commit()
                                                //手动下载资源
                                                val loadingView =
                                                    currentView.findViewById<DownloadItem>(R.id.download_item) as DownloadItem
                                                GPUImageHelper.updateFrame(
                                                    !it.frameType.equals("def"),
                                                    mAdapter,
                                                    position,
                                                    context,
                                                    it,
                                                    mImageFrameLayout,
                                                    loadingView
                                                )
                                            } else {
                                                image_frame_view.setFrameAssets(
                                                    mFrameData[position.plus(
                                                        1
                                                    )] as FrameAssetsModel
                                                )
                                            }
                                            return@let
                                        }
                                        if (it.frameType.equals("def")) {
                                            image_frame_view.setFrameAssets(it)
                                        } else {
                                            //动态平铺tiled
                                            image_frame_view.setFrameAssets(it, true)
                                        }
                                    }
                                    else -> {
                                        image_frame_view.setFrameAssets(null)
                                    }
                                }
                            }
                        }
                    }
                    PetternType.STYLE_TRANSFER -> {

                        when (position) {
                            0 -> {
                                val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
                                mGpuImageLayout.styleTransfer(bitmap)
                            }
                            else -> {
                                mStyleTransferData[position.plus(1)].let {
                                    if (it is StyleTransferAssetsModel) {
                                        if (CheckUtils.isEmpty(it.path)) {
                                            updateStyleTransferItem(position)
                                            val localAssetsSaveTime = SPManager.get(ASSETS)
                                                .getLong(it.styletransferID.toString(), 0)
                                            if (CheckUtils.isEmpty(it.path) || it.styletransferTime > localAssetsSaveTime) {
                                                SPManager.get(ASSETS).editor().putLong(
                                                    it.styletransferID.toString(),
                                                    it.styletransferTime
                                                ).commit()
                                                //手动下载资源
                                                val loadingView =
                                                    currentView.findViewById<DownloadItem>(R.id.download_item) as DownloadItem
                                                GPUImageHelper.updateStyleTransfer(
                                                    mAdapter,
                                                    position,
                                                    context,
                                                    it,
                                                    loadingView,
                                                    object : GPUImageHelper.Observer {
                                                        override fun just(data: Any) {
                                                            if (data is StyleTransferAssetsModel) {
                                                                val bitmap =
                                                                    BitmapFactory.decodeFile(data.path)
                                                                playTransfer(
                                                                    bitmap,
                                                                    data.styletransferBlendingRatio
                                                                )
                                                            }
                                                        }
                                                    }
                                                )
                                            } else {
                                                val bitmap = BitmapFactory.decodeFile(it.path)
                                                playTransfer(bitmap, it.styletransferBlendingRatio)
                                            }
                                            return@let
                                        } else {
                                            val bitmap = BitmapFactory.decodeFile(it.path)
                                            playTransfer(bitmap, it.styletransferBlendingRatio)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                onLevelSelect(currentView, lastView, data as SecondNode)
            }

            override fun onCustomClick(
                lastView: View?,
                currentView: View,
                data: BaseNode,
                position: Int,
                extra: Any?
            ) {
                //自定义风格化图片
                PictureSelectorHelper.openGallery(
                    context as Activity,
                    object : PictureSelectorHelper.Callback {
                        override fun onResult(result: String) {
                            val bitmap = if (result.contains("content")) {
                                BitmapUtils.parseBitmapFromUri(context, result)
                            } else {
                                BitmapFactory.decodeFile(result)
                            }
                            if (data is StyleTransferAssetsModel) {
                                playTransfer(
                                    TfLiteImageUtils.scaleBitmap(
                                        bitmap,
                                        StyleTransferModelExecutor.STYLE_IMAGE_SIZE
                                    ), data.styletransferBlendingRatio
                                )
                            } else if (data is TextureAssetsModel) {
                                val filter = GPUImageFilterTools.createBlendFilter(
                                    context,
                                    GPUImageAlphaBlendFilter::class.java,
                                    bitmap
                                )
                                addFilterWithProgress(filter, null)
                                renderFilter()
                            }
                        }
                    })
            }

            override fun isNeesPayMember() {
                mControlerStateLisener?.payMember()
            }
        })
        bt_copy.setOnClickListener {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText(null, tv_filter_debug.text)
            clipboard.setPrimaryClip(clipData)
            Toast.makeText(context, "已复制到剪贴板",Toast.LENGTH_SHORT).show()
        }
        tone_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                mFilterAdjuster?.let {
                    it.adjust(progress)
                    mGpuImageLayout.requestRender()
                    if(showFilterParams) {
                        tv_filter_debug.text = "${it.float[0]}  ${it.filter.javaClass.simpleName}"
                    }
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                mCurrentGpuimageFilter?.let {
                    if (mFilterAdjuster == null) {
                        mFilterAdjuster = GPUImageFilterTools.FilterAdjuster(it)
                    }
                    if (mFilterAdjuster!!.canAdjust()) {
                        val gpuImageModel = GPUImageModel()
                        val floatArray = mFilterAdjuster?.float ?: FloatArray(5)
                        gpuImageModel.apply {
                            this.type = mPetternType
                            this.filter = it
                            this.progressState = FilterProgressState(
                                tone_seekbar.progress,
                                floatArray.clone()
                            )
                            this.hasProgress = mFilterAdjuster!!.canAdjust()
                            this.position = mCurrentPosition
                        }
                        //添加调整到二级队列
                        if(mPetternType == PetternType.ADJUST) {
                            mUndoRedoHelper.addAdjust(
                                gpuImageModel,
                                mPetternType,
                                mCurrentPosition
                            )
                            resetUndoRedoState()
                        }
                    }
                }
                isEdit = true
                hasProgress = true
            }
        })
        tv_finish.setOnClickListener { saveSnapshot() }
        tv_update.setOnClickListener {
            when (mPetternType) {
                PetternType.EDIT -> {
                    mCropImageLayout?.getCropBitmap()?.let {
                        mUndoRedoHelper.recordUndoRedoState(mPetternType, it)
                        mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                        mGpuImageLayout.setImageResetMatrix(it)
                        mCropResult = it
                        renderFilter()
                    }
                }
                PetternType.FRAME -> {
                    mUndoRedoHelper.recordUndoRedoState(
                        mPetternType,
                        mCropResult,
                        mImageFrameLayout?.result() as FrameAssetsModel?,
                        doodle_controler.outputBitmap()
                    )
                    mGpuImageLayout.postFrame(
                        mImageFrameLayout?.result(),
                        mImageFrameLayout?.getFrameOffset() ?: 0
                    )
                }
                PetternType.DOODLE -> {
                    mDoodleLayout?.let {
                        //缓存笔刷队列
                        doodle_controler.saveDrawPath()
                        val paths = doodle_controler.outputBitmap()
                        mUndoRedoHelper.recordUndoRedoState(
                            mPetternType,
                            mCropResult,
                            mImageFrameLayout?.result() as FrameAssetsModel?, paths
                        )
                        // 撤销队列中的滤镜已输出修改，替换原始数据
                        mGpuImageLayout.setDrawBitmap(paths)
                        mImageFrameLayout?.setDrawBitmap(paths)
//                        updateUndoRedoLastData(bitmap)
                    }
                }
                PetternType.STYLE_TRANSFER -> {
                    //先保存撤销队列的原图，用于恢复撤销
//                    mUndoRedoHelper.clearAllState()
//                    mUndoRedoHelper.clearUndoRedoStack()
                    mGpuImageLayout.clearFilter()
                    mCropResult = mGpuImageLayout.confirmStyleTransfer()
                    mUndoRedoHelper.recordUndoRedoState(mPetternType, mCropResult, mImageFrameLayout?.result() as FrameAssetsModel?,
                        doodle_controler.outputBitmap())
                    mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                    mGpuImageLayout.setImageResetMatrix(mCropResult)
                }
                else -> {
                    if (isEdit) {
                        mUndoRedoHelper.recordUndoRedoState(
                            mPetternType, null,
                            mImageFrameLayout?.result() as FrameAssetsModel?,
                            doodle_controler.outputBitmap()
                        )
                    }
                }
            }
            //记录上一次滤镜参数状态，点x还原
//            mUndoRedoHelper.recordLastFilterState()
            onBackInit()
            mUndoRedoHelper.resetRedoState()
            resetUndoRedoState()

            isEdit = false
            hasProgress = false
        }
        //返回操作
        iv_close.setOnClickListener {
            exit()
        }
        //撤销
        iv_undo.setOnClickListener {
            if(mUndoRedoHelper.isUndoType(PetternType.DOODLE)){
                doodle_controler.undo()
            }
            tone_seekbar.visibility = if(mPetternType == PetternType.ADJUST) View.VISIBLE else View.GONE
//            mUndoRedoHelper.clearAllFilter()
            val undeRedoModel = if(mPetternType == PetternType.ADJUST) mUndoRedoHelper .undoSecond() else mUndoRedoHelper.undo()
            when (undeRedoModel.type) {
                PetternType.NORMAL -> {
                    val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
                    mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                    mGpuImageLayout.setImageResetMatrix(bitmap)
                    mGpuImageLayout.postFrame(null, 0)
                    mGpuImageLayout.setDrawBitmap(null)
                    doodle_controler.undo()
                    mCropResult = bitmap
                    mCropImageLayout?.setImageBitmap(mCropResult)
                }
                else -> {
                    undeRedoModel.bitmap?.let {
                        //撤销裁剪，需要初始化裁剪原图
                        if(undeRedoModel.type == PetternType.EDIT){
                            mCropResult = it
                            mCropImageLayout?.setImageBitmap(mCropResult)
                        }
                        mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                        mGpuImageLayout.setImageResetMatrix(it)
                    }?:let {
                        //TODO 修复裁剪后组合操作，GPUimageLayout原图不对
//                        mUndoRedoHelper.getLastRedoState()?.bitmap?.let {
                        if(undeRedoModel.type == PetternType.EDIT) {
                            mCropResult.let {
//                            val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
                                mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                                mGpuImageLayout.setImageResetMatrix(it)
                            }
                        }
                    }
                    undeRedoModel.frameAssetsModel?.let {
                        mGpuImageLayout.postFrame(
                            it,
                            DensityUtil.dp2Px(44f)
                        )
                        mImageFrameLayout?.setFrameAssets(it)
                    } ?: let {
                        mGpuImageLayout.postFrame(
                            null,
                            0
                        )
                        mImageFrameLayout?.setFrameAssets(null)
                    }
                    undeRedoModel.paths?.let {
                        mGpuImageLayout.setDrawBitmap(it)
                    } ?: let {
                        mGpuImageLayout.setDrawBitmap(null)
                    }
                }
            }
            val gpuImageFilter = undeRedoModel.getGPUImageFilterGroup()
            mGpuImageLayout.mGPUImageView.filter = gpuImageFilter
            mGpuImageLayout.requestRender()

            if(mPetternType == PetternType.ADJUST) {
                updateProgressAndPosition(undeRedoModel.filter)
            }
//            mUndoRedoHelper.backLastState()
            resetUndoRedoState()
        }
        //重置撤销
        iv_redo.setOnClickListener {
            if(mUndoRedoHelper.isRedoType(PetternType.DOODLE)){
                doodle_controler.redo()
            }
            tone_seekbar.visibility = if(mPetternType == PetternType.ADJUST) View.VISIBLE else View.GONE
            val undeRedoModel = if(mPetternType == PetternType.ADJUST) mUndoRedoHelper.redoSecond() else mUndoRedoHelper.redo()
            undeRedoModel.bitmap?.let {
                mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                mGpuImageLayout.setImageResetMatrix(it)
                //重置裁剪，需要初始化裁剪原图
                if(undeRedoModel.type == PetternType.EDIT){
                    mCropResult = it
                    mCropImageLayout?.setImageBitmap(mCropResult)
                }
            }?:let {
//                mUndoRedoHelper.getLastUndoState()?.bitmap?.let {
//                    val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
//                    mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
//                    mGpuImageLayout.setImageResetMatrix(bitmap)
//                }
                if(undeRedoModel.type == PetternType.EDIT) {
                    mCropResult.let {
//                            val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
                        mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                        mGpuImageLayout.setImageResetMatrix(it)
                    }
                }
            }
            undeRedoModel.frameAssetsModel?.let {
                mGpuImageLayout.postFrame(
                    it,
                    DensityUtil.dp2Px(44f)
                )
                mImageFrameLayout?.setFrameAssets(it)
            }?: let {
                mGpuImageLayout.postFrame(
                    null,
                    0
                )
                mImageFrameLayout?.setFrameAssets(null)
            }

            undeRedoModel.paths?.let {
                mGpuImageLayout.setDrawBitmap(it)
            }
            val gpuImageFilter = undeRedoModel.getGPUImageFilterGroup()
            mGpuImageLayout.mGPUImageView.filter = gpuImageFilter
            mGpuImageLayout.requestRender()

            if(mPetternType == PetternType.ADJUST) {
                updateProgressAndPosition(undeRedoModel.filter)
            }
//            mUndoRedoHelper.backLastState()
            resetUndoRedoState()
        }
        //逆时针旋转90度
        iv_crop_rotate.setOnClickListener {
            mCropImageLayout?.cropRotate()
        }
        //水平翻转
        iv_crop_overturn.setOnClickListener {
            mCropImageLayout?.overturnBitmap()
        }
    }


    /**
     * 撤销重置后恢复进度条以及位置状态
     */
    private fun updateProgressAndPosition(filterList: MutableList<GPUImageModel>) {
        if(filterList.size > 0 ){
            val position = filterList.last().position
            val progressState = filterList.last().progressState
            val filter = filterList.last().filter

            if (filterList.last().hasProgress) {
                tone_seekbar.visibility = View.VISIBLE
                filter?.let { gpuImageFilter ->

                    if(progressState !=null )
//                        && (mCurrentPosition == position || gpuImageFilter.javaClass.simpleName.equals(mFilterAdjuster?.filter?.javaClass?.simpleName))
                        {
                        mFilterAdjuster = GPUImageFilterTools.FilterAdjuster(gpuImageFilter)
                        val progress = progressState.progress
                        tone_seekbar.progress = progress
                        mFilterAdjuster?.adjust(progress)
                        mAdapter.setSecondPosition(position.plus(1))
                        mAdapter.notifyDataSetChanged()
                    }
                }
            }
        }else{
            //没有滤镜重置状态
            tone_seekbar.visibility = View.GONE
            mAdapter.setSecondPosition(0)
            mAdapter.notifyDataSetChanged()
        }
    }

    /**
     * ai风格化
     */
    fun playTransfer(styleBitmap: Bitmap, styleContentRatio: Float) {
        GlobalScope.launch {
            var result: ModelExecutionResult? = null
            withContext(Dispatchers.IO) {
                result = mStyleTransferModelExecutor.selectStyle(
                    styleBitmap,
                    styleContentRatio,
                    context
                )
            }
            withContext(Dispatchers.Main) {
                result?.let {
                    mGpuImageLayout.styleTransfer(it.styledImage)
                }
            }
        }
    }

    private fun exit() {
        isEdit = false
//        clearFilterByType()
        requestLastUndoState()
        onBackInit()
        resetUndoRedoState()
        mControlerStateLisener?.cancel()
        hasProgress = false
    }

    /**
     * 取消操作，回退到上次撤销队列
     */
    private fun requestLastUndoState() {
        val undoModel = mUndoRedoHelper.getLastUndoState()
        undoModel?.let { undeRedoModel->
            undeRedoModel.bitmap?.let {
                mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
                mGpuImageLayout.setImageResetMatrix(it)
            }
            undeRedoModel.frameAssetsModel?.let {
                mGpuImageLayout.postFrame(
                    it,
                    DensityUtil.dp2Px(44f)
                )
            }
            undeRedoModel.paths?.let {
                mGpuImageLayout.setDrawBitmap(it)
            }
            val gpuImageFilter = undeRedoModel.getGPUImageFilterGroup()
            mGpuImageLayout.mGPUImageView.filter = gpuImageFilter
            mGpuImageLayout.requestRender()
            //取消操作后，同步当前操作状态
            mUndoRedoHelper.backLastState()
        }?:let {
            mGpuImageLayout.mGPUImageView.filter = GPUImageFilter()
            mGpuImageLayout.requestRender()
            mUndoRedoHelper.backLastState()
        }
    }

    /**
     * 更新边框
     */
    private fun updateFrameItem(position: Int) {
        mFrameData[position.plus(1)].let {
            if (it is FrameAssetsModel) {
                val model = PetternAssetsHelper(context).parseFrameModel(it.frameId.toString())
                model?.let { asset ->
                    it.left = asset.left
                    it.right = asset.right
                    it.up = asset.up
                    it.down = asset.down
                    it.up_left = asset.up_left
                    it.up_right = asset.up_right
                    it.down_left = asset.down_left
                    it.down_right = asset.down_right
                    it.frameZip = asset.frameZip
                }
            }
        }
    }

    /**
     * 更新风格迁移
     */
    private fun updateStyleTransferItem(position: Int) {
        mStyleTransferData[position.plus(1)].let {
            if (it is StyleTransferAssetsModel) {
                val model =
                    PetternAssetsHelper(context).parseStyleTransferModel(it.styletransferID.toString())
                model?.let { asset ->
                    it.path = asset.path
                }
            }
        }
    }

    /**
     * 重制视图
     */
    private fun resetBitmap() {
        val bitmap = BitmapUtils.parseBitmapFromUri(context, mUriPath)
        mGpuImageLayout.mGPUImageView.gpuImage.deleteImage()
        mGpuImageLayout.mGPUImageView.setImage(bitmap)
    }

    /**
     * 刷新撤销重置撤销状态
     */
    private fun resetUndoRedoState() {
        iv_undo.isEnabled = if(mPetternType == PetternType.ADJUST) mUndoRedoHelper.isCanSecondUndo() else mUndoRedoHelper.isCanUndo()
        iv_redo.isEnabled = if(mPetternType == PetternType.ADJUST) mUndoRedoHelper.isCanSecondRedo() else mUndoRedoHelper.isCanRedo()
    }

    /**
     * 显示/隐藏撤销
     */
    private fun checkUndoRedoVisible(visible: Boolean) {
        bg_undo.visibility = if (visible) View.VISIBLE else View.INVISIBLE
        bg_redo.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    /**
     * 选中滤镜
     */
    private fun onSelectFilter(it: GPUImageFilter? = null, position: Int) {
        it?.let {
            addFilterWithProgress(it, position)
            renderFilter(true)
        } ?: let {
            resetBitmap()
            tone_seekbar.visibility = View.GONE
        }

    }

    /**
     * 点击滤镜
     */
    private fun addFilterWithProgress(it: GPUImageFilter, position: Int?) {
        mFilterAdjuster = GPUImageFilterTools.FilterAdjuster(it)
        val recordProgress =
            mUndoRedoHelper.getLastFilterProgress(it)?.progress ?: mFilterAdjuster!!.defaultPercent

        val gpuImageModel = GPUImageModel()
        gpuImageModel.apply {
            this.type = mPetternType
            this.filter = it
            this.progressState =
                FilterProgressState(recordProgress, mFilterAdjuster?.float ?: FloatArray(5))
            this.hasProgress = mFilterAdjuster!!.canAdjust()
        }
        mUndoRedoHelper.addFilter(gpuImageModel, mPetternType, position)
        mFilterAdjuster?.adjust(recordProgress)
        tone_seekbar.progress = recordProgress
        tone_seekbar.visibility = View.VISIBLE
    }

    /**
     * 渲染滤镜
     */
    private fun renderFilter(isInitFilters: Boolean = false) {
        val filter = mUndoRedoHelper.getFilterGroup()
        mGpuImageLayout.mGPUImageView.filter = filter
        mGpuImageLayout.requestRender()
    }

    /**
     * 二级套娃选中
     */
    private fun onLevelSelect(currentView: View, lastView: View?, secondNode: SecondNode) {
        currentView.setBackgroundResource(R.drawable.bg_pettern_item_selected)
        lastView?.let { it.background = null }
    }

    /**
     * 重置底部居中选中的文字
     */
    private fun resetBottomCenterText() {
        tv_center_title.text = ""
        tab_layout.visibility = View.GONE
    }

    /**
     * 返回操作
     */
    private fun onBackInit() {
        if (mPetternType == PetternType.NORMAL) {
            val dialog = AlertDialog.Builder(context)
                .setTitle("退出编辑？")
                .setMessage("如果退出，您所做的修改将被丢弃")
                .setPositiveButton(
                    "取消"
                ) { _, _ -> }
                .setNegativeButton(
                    "确认"
                ) { _, _ -> (context as Activity).finish() }.create()
            if (iv_undo.isEnabled) {
                dialog.show()
            } else {
                (context as Activity).finish()
            }
        }
        checkUndoRedoVisible(true)
        rv_controler.adapter = mNormalAdapter
        mNormalAdapter.setList(mPetternData)
        mCropImageLayout?.visibility = View.GONE
        mImageFrameLayout?.visibility = View.GONE
        mGpuImageLayout.visibility = View.VISIBLE
        mGpuImageLayout.finish()
        tone_seekbar.visibility = View.GONE
        doodle_controler.visibility = View.GONE
        mDoodleLayout?.visibility = View.GONE
        checkUpdateButton(false)
        mPetternType = PetternType.NORMAL
        resetBottomCenterText()
        //隐藏裁剪头部
        expand_header.visibility = View.GONE
    }

    /**
     * 点击编辑
     */
    private fun onClickEdit(position: Int) {
        mCropImageLayout?.let { cropImageLayout ->
            cropImageLayout.visibility = View.VISIBLE
            mGpuImageLayout.visibility = View.GONE
            cropImageLayout.checkCropMode(mCropData[position.plus(1)].ratio)
        }

    }

    /**
     * 点击边框
     */
    private fun onClickFrame() {
        mImageFrameLayout?.let {
            it.visibility = View.VISIBLE
            mGpuImageLayout.visibility = View.GONE
            it.setPetternBitmap(mGpuImageLayout.capture())
            //每次进入边框编辑，清除缓存边框
            it.setFrame(null)
        }
    }

    private fun initView() {
        View.inflate(context, R.layout.view_pettern_controler, this)

        val manager = LinearLayoutManager(context)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        rv_controler.layoutManager = manager
        rv_controler.adapter = mNormalAdapter
        rv_controler.addItemDecoration(LinearListItemDecoration("#1C1C1C", 1))

        iv_undo.isEnabled = false
        iv_redo.isEnabled = false
    }

    /**
     * 绑定渲染组件
     * @param gpuImageLayout
     */
    fun bindGpuImageView(gpuImageLayout: GPUImageLayout, uri: String? = null) {
        mGpuImageLayout = gpuImageLayout
        mUriPath = uri
        mAdapter.setImageUri(gpuImageLayout.context, uri)
        mUriPath?.let {
            mGpuImageLayout.setUri(it)
            mCropResult = BitmapUtils.parseBitmapFromUri(context, mUriPath)
        }

        initFilterList()
    }

    /**
     * 绑定裁剪组件
     */
    fun bindCropImageLayout(cropImageLayout: CropImageLayout) {
        mCropImageLayout = cropImageLayout
        mUriPath?.let {
            mCropImageLayout?.setImageBitmap(it)
        }
    }

    /**
     * 绑定边框组件
     */
    fun bindFrameLayout(imageFrameLayout: ImageFrameLayout) {
        mImageFrameLayout = imageFrameLayout
    }

    /**
     * 绑定
     */
    fun bindDoodleLayout(doodleView: DrawLayout, doodleAdjuster: DoodleAdjuster) {
        mDoodleLayout = doodleView
        mDoodleLayout?.let {
            doodle_controler.bindDoodleLayout(it)
        }
        doodleAdjuster.let {
            doodle_controler.bindDoodleAdjuster(it)
        }
        doodle_controler.bindTabLayout(tab_layout)
    }

    fun setInitPosition(position: Int) {
        mInitPisition = position
        if (mInitPisition >= 0) {
            onSelectFirstLevel(mInitPisition)
        }
    }

    fun showFilterParams(showFilterParams: Boolean){
        this.showFilterParams = showFilterParams
        group_debug.visibility = if(showFilterParams) View.VISIBLE else View.GONE
    }

    /**
     * 保存图片
     */
    private fun saveSnapshot() {
        val saveBitmap = BitmapUtil.convertViewToBitmap(mGpuImageLayout)
        saveBitmap?.let {
            mControlerStateLisener?.finish(it)
        }
    }

    /**
     * @param isChecked true显示更新操作，false显示完成操作
     * 切换更新按钮
     */
    private fun checkUpdateButton(isChecked: Boolean) {
        tv_update.visibility = if (isChecked) View.VISIBLE else View.INVISIBLE
        tv_finish.visibility = if (isChecked) View.INVISIBLE else View.VISIBLE
    }

    fun setControlerStateListener(l: OnContralerStateLisener) {
        mControlerStateLisener = l
    }

}

/**
 * 可扩展的适配器（套娃数据支持）
 */
class PetternExpandAdapter : BaseNodeAdapter() {

    private val mFirstProvider = FirstProvider()
    private val mSecondProvider = SecondProvider()

    init {
        addNodeProvider(mFirstProvider)
        addNodeProvider(mSecondProvider)
    }

    override fun getItemType(data: List<BaseNode>, position: Int): Int {
        val node = data[position]
        if (node is FirstNode) {
            return 1
        } else if (node is SecondNode) {
            return 2
        }
        return -1
    }

    /**
     * 回调二级事件
     */
    fun addNodeItemCallback(callback: NodeItemClickCallback) {
        mSecondProvider.addNodeItemCallback(callback)
    }

    fun addLevelItemCallback(callback: FirstLevelClickCallback) {
        mFirstProvider.addLevelItemCallback(callback)
    }

    fun setImageUri(context: Context, uri: String?) {
        mFirstProvider.setImageUri(context, uri)
        mSecondProvider.setImageUri(context, uri)
    }

    fun setSecondPosition(position: Int) {
        mSecondProvider.setLastPosition(position)
    }

}

/**
 * 一级数据提供者
 */
class FirstProvider : BaseEditorProvider() {

    private var mFirstCallback: FirstLevelClickCallback? = null

    override fun convert(helper: BaseViewHolder, item: BaseNode) {
        if (item is FirstNode) {
            helper.setText(R.id.tv_title, item.title)
            item.icon?.let {
                helper.setImageResource(R.id.iv_source, it)
            } ?: let {
                val imageView = helper.getView<ImageView>(R.id.iv_source)
                Glide.with(imageView).load(mImageBitmap).into(imageView)
            }
        }
    }

    override val itemViewType: Int
        get() = 1

    override val layoutId: Int
        get() = R.layout.item_pettern_expand_first

    override fun onClick(helper: BaseViewHolder, view: View, data: BaseNode, position: Int) {
        getAdapter()?.expandOrCollapse(position)
        mFirstCallback?.onLevelClick(view, data, position)
    }

    fun addLevelItemCallback(callback: FirstLevelClickCallback) {
        mFirstCallback = callback
    }

}

/**
 * 二级数据提供者
 */
class SecondProvider : BaseEditorProvider() {

    private var mCallback: NodeItemClickCallback? = null
    private var mLastView: View? = null
    private var mListPosition = 0

    override fun convert(helper: BaseViewHolder, item: BaseNode) {
        if (item is SecondNode) {
            helper.setIsRecyclable(item.type == PetternType.FILTER)
            helper.setText(R.id.tv_title, item.title)
            helper.setVisible(R.id.iv_vip_tip, false)

            when (item.type) {
                PetternType.ADJUST, PetternType.EDIT, PetternType.NORMAL, PetternType.FOCUS -> {
                    item.icon?.let {
                        helper.setImageResource(R.id.iv_source, it)
                    }
                }
                PetternType.FILTER, PetternType.TEXTURE -> {
                    //滤镜类型 预览效果
                    mImageBitmap.let { image ->
                        if (item is TextureAssetsModel) {
                            helper.setText(R.id.tv_title, item.textureTitle)
                            if (item.textureIcon != 0) {
                                //自定义
                                setImageIcon(item.textureIcon, helper)
                            } else {
                                helper.setVisible(R.id.iv_vip_tip, item.vipResources != 0)
                                val imageView = helper.getView<ImageView>(R.id.item_gpu_image)
                                var bitmap: Bitmap? = null
                                helper.setVisible(R.id.download_item, CheckUtils.isEmpty(item.path))
                                GlobalScope.launch {
                                    withContext(Dispatchers.IO) {
                                        if (!TextUtils.isEmpty(item.path)) {
                                            bitmap = Glide.with(imageView).asBitmap()
                                                .placeholder(RandomColorBg.colorDrawable)
                                                .load(item.path)
                                                .submit().get()
                                            item.filter = GPUImageFilterTools.createBlendFilter(
                                                context,
                                                GPUImageAlphaBlendFilter::class.java,
                                                bitmap
                                            )
                                        }
                                    }
                                    withContext(Dispatchers.Main) {
                                        Glide.with(imageView).load(item.iconUrl)
                                            .placeholder(RandomColorBg.colorDrawable)
                                            .into(imageView)
                                    }
                                }
                            }
                        } else {
                            GlobalScope.launch {

                                var newBitmap: Bitmap? = null
                                withContext(Dispatchers.IO) {
                                    val gpuImage = GPUImage(context)
                                    gpuImage.setImage(image)
                                    gpuImage.setFilter(item.filter)

                                    val bitmap =
                                        if (item.filter != null) gpuImage.bitmapWithFilterApplied
                                        else image

                                    bitmap?.let {
                                        newBitmap = Bitmap.createScaledBitmap(it, 100, 100, true)
                                    }
                                }
                                withContext(Dispatchers.Main) {
                                    val imageView = helper.getView<ImageView>(R.id.item_gpu_image)
                                    imageView.setImageBitmap(newBitmap)
                                }
                            }
                        }
                    }
                }
                PetternType.FRAME -> {
                    when (item) {
                        is FrameResNode -> {
                            val imageView = helper.getView<ImageView>(R.id.item_gpu_image)
                            imageView.scaleType = ImageView.ScaleType.FIT_XY
                            Glide.with(imageView).load(item.icon)
                                .placeholder(RandomColorBg.colorDrawable).into(imageView)
                            helper.setVisible(R.id.item_gpu_image, true)
                            helper.setVisible(R.id.iv_source, false)
                        }
                        is FrameAssetsModel -> {
                            helper.setVisible(R.id.iv_vip_tip, item.vipResources != 0)
                            val imageView = helper.getView<ImageView>(R.id.item_gpu_image)
                            imageView.scaleType = ImageView.ScaleType.FIT_XY
                            Glide.with(imageView).load(item.imageUrl)
                                .placeholder(RandomColorBg.colorDrawable).into(imageView)
                            helper.setVisible(R.id.item_gpu_image, true)
                            helper.setVisible(R.id.iv_source, false)
                            helper.setVisible(R.id.download_item, CheckUtils.isEmpty(item.left))
                        }
                        else -> {
                            val imageView = helper.getView<ImageView>(R.id.iv_source)
                            imageView.setImageResource(item.icon!!)
                            helper.setVisible(R.id.item_gpu_image, false)
                            helper.setVisible(R.id.iv_source, true)
                            helper.setVisible(R.id.download_item, false)
                        }
                    }
                }
                PetternType.STYLE_TRANSFER -> {
                    if (item is StyleTransferAssetsModel) {
                        helper.setText(R.id.tv_title, item.styletransferTitle)
                        helper.setVisible(R.id.iv_vip_tip, item.vipResources != 0)
                        helper.setVisible(
                            R.id.download_item,
                            (item.styletransferID > 0 && CheckUtils.isEmpty(item.path))
                        )
                        if (!TextUtils.isEmpty(item.iconUrl)) {
                            setBackground(R.id.item_gpu_image, item.iconUrl, null, helper)
                        } else if (item.styleIcon != 0) {
                            //自定义
                            setImageIcon(item.styleIcon, helper)
                        } else {
                            //无ai特效
                            mImageBitmap?.let {
                                setBackground(R.id.item_gpu_image, null, it, helper)
                            }
                        }
                    }
                }
            }
        }

        if (mListPosition == helper.adapterPosition) {
            helper.itemView.setBackgroundResource(R.drawable.bg_pettern_item_selected)
            mLastView = helper.itemView
        } else {
            helper.itemView.setBackgroundResource(0)
        }
    }

    private fun setImageIcon(icon: Int?, helper: BaseViewHolder) {
        val imageView = helper.getView<ImageView>(R.id.iv_source)
        icon?.let {
            imageView.setImageResource(it)
        }
        helper.setVisible(R.id.iv_source, true)
        helper.setVisible(R.id.item_gpu_image, false)
    }

    /**
     * @param imageViewId 背景图id
     * @param path 图片路径
     * @param bitmap 图片
     */
    private fun setBackground(
        imageViewId: Int,
        path: String?,
        bitmap: Bitmap?,
        helper: BaseViewHolder
    ) {
        val imageView = helper.getView<ImageView>(imageViewId)
        imageView.scaleType = ImageView.ScaleType.FIT_XY
        path?.let {
            Glide.with(imageView).load(it)
                .placeholder(RandomColorBg.colorDrawable).into(imageView)
        }
        bitmap?.let {
            Glide.with(imageView).load(it)
                .placeholder(RandomColorBg.colorDrawable).into(imageView)
        }
        helper.setVisible(imageViewId, true)
        helper.setVisible(R.id.iv_source, false)
    }

    override val itemViewType: Int
        get() = 2

    override val layoutId: Int
        get() = R.layout.item_pettern_expand_second

    /**
     * 需要购买会员
     */
    private fun isNeedMemberPay(data: BaseNode): Boolean{
        if(context is ImagePetternActivity){
            (context as ImagePetternActivity).mConfig?.isMember()?.let {
                //不是会员并且是付费资源需要购买会员
                if(!it){
                    if(data is FrameAssetsModel){
                        return data.vipResources ==1
                    }
                    if(data is TextureAssetsModel){
                        return data.vipResources ==1
                    }
                    if(data is StyleTransferAssetsModel){
                        return data.vipResources ==1
                    }
                }
            }
        }
        return false
    }

    private var mEventTime: Long = 0
    override fun onClick(helper: BaseViewHolder, view: View, data: BaseNode, position: Int) {
        super.onClick(helper, view, data, position)
        if(isNeedMemberPay(data)){
            mCallback?.isNeesPayMember()
            return
        }
        val currentTime = System.currentTimeMillis()
        if (data is SecondNode && mListPosition != position) {
            //风格迁移2秒内禁止连续切换
            if (data.type == PetternType.STYLE_TRANSFER && currentTime.minus(mEventTime) < 2000) {
                return
            }
            mEventTime = currentTime

            val filter = if (data is TextureNode) data.lessFilter else data.filter
            val localAssetsSaveTime = if (data is TextureAssetsModel) SPManager.get(ASSETS)
                .getLong(data.textureId.toString(), 0) else 0
            if (data is TextureAssetsModel && data.textureId > 0 && (filter == null || data.textureUpdateTime > localAssetsSaveTime)) {
                val loadingView = view.findViewById<DownloadItem>(R.id.download_item)
                SPManager.get(ASSETS).editor()
                    .putLong(data.textureId.toString(), data.textureUpdateTime).commit()
                GPUImageHelper.updateTexture(
                    getAdapter(),
                    position,
                    context,
                    data,
                    loadingView,
                    object : GPUImageHelper.Observer {
                        override fun just(juster: Any) {
                            if (juster is TextureAssetsModel) {
                                mCallback?.onNodeItemClick(
                                    mLastView,
                                    view,
                                    juster,
                                    position.minus(1),
                                    juster.filter
                                )
                                mLastView = view
                                mListPosition = position
                            }
                        }
                    })
            } else {
                if ((data.type == PetternType.STYLE_TRANSFER || data.type == PetternType.TEXTURE) && position == 0) {
                    mCallback?.onCustomClick(mLastView, view, data, position.minus(1), filter)
                } else {
                    mCallback?.onNodeItemClick(mLastView, view, data, position.minus(1), filter)
                    mLastView = view
                    mListPosition = position
                }
            }

        }
    }

    /**
     * child click
     */
    fun addNodeItemCallback(callback: NodeItemClickCallback) {
        mCallback = callback
    }

    fun setLastPosition(position: Int) {
        mListPosition = position
    }

}

/**
 * 一级Node数据类
 */
class FirstNode(
    private val mChildNodeList: MutableList<BaseNode>?,
    val title: String, val icon: Int? = null, val type: PetternType = PetternType.NORMAL
) : BaseExpandNode() {

    init {
        //默认不展开
        isExpanded = false
    }

    override val childNode: MutableList<BaseNode>?
        get() = mChildNodeList
}

/**
 * 裁剪二级数据类
 */
class CropNode(title: String, val ratio: Float, cropIcon: Int? = null, type: PetternType) :
    SecondNode(title, null, cropIcon, type)

/**
 * 纹理二级数据
 */
class TextureNode(
    title: String,
    filter: GPUImageFilter?,
    textureIcon: Int?,
    type: PetternType,
    val lessFilter: GPUImageFilter
) :
    SecondNode(title, filter, textureIcon, type)

class FrameNode(
    title: String,
    filter: GPUImageFilter?,
    frameIcon: Int?,
    type: PetternType,
    val frameId: Int?
) :
    SecondNode(title, filter, frameIcon, type)

class FrameResNode(
    title: String, filter: GPUImageFilter?, frameIcon: Int?, type: PetternType,
    val angleLeftTopRes: Int, val angleLeftBottomRes: Int,
    val angleRightTopRes: Int, val angleRightBottomRes: Int,
    val frameLeftRes: Int, val frameTopRes: Int, val frameRightRes: Int, val frameBottomRes: Int
) :
    SecondNode(title, filter, frameIcon, type)

/**
 * 二级Node数据类
 */
open class SecondNode(
    val title: String,
    var filter: GPUImageFilter?,
    val icon: Int? = null,
    val type: PetternType = PetternType.NORMAL
) : BaseNode() {
    //套娃数据，不需要继续嵌套返回null就可以了
    override val childNode: MutableList<BaseNode>?
        get() = null
}

/**
 * 可扩展Node事件回传
 */
interface NodeItemClickCallback {

    /**
     * @param data 数据
     * @param position 当前索引。注意：二级Node索引从1开始计算。
     * @param extra 开放一个扩展参数，用于回传滤镜
     */
    fun onNodeItemClick(
        lastView: View? = null,
        currentView: View,
        data: BaseNode,
        position: Int,
        extra: Any? = null
    )

    fun onCustomClick(
        lastView: View? = null,
        currentView: View,
        data: BaseNode,
        position: Int,
        extra: Any? = null
    )

    fun isNeesPayMember()
}

interface FirstLevelClickCallback {

    fun onLevelClick(view: View, data: BaseNode, position: Int)
}

interface OnContralerStateLisener {
    fun finish(bitmap: Bitmap)
    fun cancel()
    fun payMember()
}
