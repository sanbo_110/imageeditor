package com.tuyue.common_sdk.widget
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.divyanshu.draw.widget.MyPath
import com.divyanshu.draw.widget.PaintOptions
import com.tuyue.common_sdk.model.FrameAssetsModel
import com.tuyue.common_sdk.tools.GPUImageFilterTools
import com.xinlan.imageeditlibrary.editimage.utils.BitmapUtils
import jp.co.cyberagent.android.gpuimage.GPUImageView
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import java.util.LinkedHashMap

/**
 * create by guojian
 * Date: 2020/12/24
 */
class GPUImageLayout : FrameLayout {

    //边框id
    private var mFrameId: Int? = null
    private var mFrameView: ImageView = ImageView(context)
    private var mImageFrameView: ImageFrameView = ImageFrameView(context)
    var mGPUImageView: GPUImageView = GPUImageView(context)
    private var mStyleTransferView: ImageView = ImageView(context)
    var mFrameOffset: Int = 0
    private var mUri = ""
    var mInitBounds: Point? = null
    private val mPetternDrawView = ImageView(context)
    private var mBitmap: Bitmap? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    init {
        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        mGPUImageView.layoutParams = params
        addView(mGPUImageView)

        mStyleTransferView.layoutParams = params
        addView(mStyleTransferView)

        mFrameView.layoutParams = params
        mFrameView.scaleType = ImageView.ScaleType.FIT_XY
        addView(mFrameView)

        mPetternDrawView.layoutParams = params
        addView(mPetternDrawView)

        mImageFrameView.layoutParams = params
        mImageFrameView.setScaleType(ImageView.ScaleType.FIT_XY)
        addView(mImageFrameView)
    }

    fun setUri(uri: String) {
        mUri = uri
    }

    /**
     * 设置边框
     * @param secondNode 边框资源模型
     * @param frameOffset 偏移量
     */
    fun postFrame(secondNode: SecondNode?, frameOffset: Int) {
        if(secondNode == null && frameOffset ==0 ){

        }
        post {
            val params = mGPUImageView.layoutParams as LayoutParams
            if(frameOffset > 0 && secondNode != null){
                mFrameOffset = frameOffset
                params.setMargins(frameOffset, frameOffset, frameOffset, frameOffset)
                mImageFrameView.visibility = View.VISIBLE
            } else {
                mFrameOffset = 0
                params.setMargins(0, 0, 0, 0)
                mImageFrameView.visibility = View.INVISIBLE
            }
            mGPUImageView.layoutParams = params
            mPetternDrawView.layoutParams = params
            mStyleTransferView.layoutParams = params

            measureBounds(mBitmap?:capture())

            secondNode?.let {
                if (it is FrameResNode) {
                    mImageFrameView.setFrameResouce(it)
                }
                if (it is FrameAssetsModel) {
                    mImageFrameView.setFrameAssets(
                        it,
                        !TextUtils.equals(it.frameType, "def"),
                        mFrameOffset
                    )
                }
            }
        }
    }

    fun setDrawBitmap(bitmap: Bitmap?){
        mPetternDrawView.visibility = if(bitmap==null) View.INVISIBLE else View.VISIBLE
        mPetternDrawView.setImageBitmap(bitmap)
    }

    fun hasFrame(): Boolean{
        return mFrameOffset > 0
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val ract = Rect(0, 0, width, height)
        mFrameId?.let {
            val frameBitmap = BitmapFactory.decodeResource(resources, it)
            canvas?.drawBitmap(frameBitmap, ract, ract, Paint())
        }
    }

    /**
     * 预览风格迁移效果
     */
    fun styleTransfer(bitmap: Bitmap) {
        mStyleTransferView.visibility = View.VISIBLE
        mGPUImageView.visibility = View.INVISIBLE
        mStyleTransferView.setImageBitmap(bitmap)
    }

    fun finish() {
        mStyleTransferView.visibility = View.INVISIBLE
        mGPUImageView.visibility = View.VISIBLE
    }


    /**
     * 设置
     */
    fun setImageResetMatrix(bitmap: Bitmap) {
        mBitmap = bitmap
        post {
            if (mInitBounds == null) {
                mInitBounds = Point(width, height)
            }
            measureBounds(bitmap)
            mGPUImageView.setImage(bitmap)
        }
    }

    private fun measureBounds(bitmap: Bitmap) {
        var resetWidght = 0
        var resetHeight = 0
        mInitBounds?.let {

            val widgetWidth = it.x
            val widgetHeight = it.y

            if (bitmap.width.toFloat() / bitmap.height.toFloat() > (widgetWidth - 2 * mFrameOffset).toFloat() / (widgetHeight - 2 * mFrameOffset).toFloat()) {
                resetWidght = widgetWidth
                resetHeight = (resetWidght - 2 * mFrameOffset).times(bitmap.height)
                    .div(bitmap.width) + 2 * mFrameOffset
            } else {
                resetHeight = widgetHeight
                resetWidght = (resetHeight - 2 * mFrameOffset).times(bitmap.width)
                    .div(bitmap.height) + 2 * mFrameOffset
            }
            val newParams = layoutParams
            newParams.width = resetWidght
            newParams.height = resetHeight
            layoutParams = newParams

        }
    }

    fun confirmStyleTransfer(): Bitmap {
        val style = (mStyleTransferView.drawable as BitmapDrawable).bitmap
        return style
    }

    fun capture(): Bitmap {
        return try {
            mGPUImageView.capture()
        } catch (e: Exception) {
            BitmapUtils.parseBitmapFromUri(context, mUri)
        }
    }

    @Deprecated("")
    fun initFilters(){
        val filters = mGPUImageView.filter
        if(filters is GPUImageFilterGroup){
            filters.filters.forEach {
                val adjuster = GPUImageFilterTools.FilterAdjuster(it)
                if(adjuster.canAdjust()){
                    adjuster.adjust(adjuster.defaultPercent)
                }
            }
        }
    }

    fun requestRender() {
        mGPUImageView.requestRender()
    }

    fun clearFilter() {
        mGPUImageView.filter = GPUImageFilter()
        mGPUImageView.requestRender()
    }
}