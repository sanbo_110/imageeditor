package com.tuyue.common_sdk.widget

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.divyanshu.draw.widget.DrawView
import com.tuyue.common_sdk.R
import com.tuyue.common_sdk.tools.BitmapUtil
import com.tuyue.core.util.MFBezierCurvesTool
import com.xinlan.imageeditlibrary.editimage.utils.BitmapUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.math.*


/**
 * Created by wensefu on 17-3-21.
 */
class PaletteView : View {
    private var mDrawPaint = Paint()
    private var mEraserPaint = Paint()
    private var mPathPaint = Paint(Paint.FILTER_BITMAP_FLAG)
    private var mPath: Path? = null
    private var mLastX = 0f
    private var mLastY = 0f
    private var mLastMoveX = 0f
    private var mLastMoveY = 0f
    private var mBufferBitmap: Bitmap? = null
    private var mBufferCanvas: Canvas? = null

    //path effect bitmap如果加滤镜，会影响到笔刷渲染，需要单独管理
    private var mPathEffectBufferBitmap: Bitmap? = null
    private var mPathEffectBufferCanvas: Canvas? = null

    private var mMovingBitmap: Bitmap? = null
    private var mMovingCanvas: Canvas? = null
    var mDrawingList: MutableList<DrawingInfo>? = null
    private var mRemovedList: MutableList<DrawingInfo>? = null
    private var mXferModeClear: Xfermode? = null
    private var mXferModeDraw: Xfermode? = null
    var mMaskFilter: BlurMaskFilter? = null
    var penSize = 0
        private set
    var eraserSize = 0
    private var mPenAlpha = 255
    private var mCanEraser = false
    private var mCallback: Callback? = null
    private var mTouchAction: DrawView.TouchAction? = null

    //    private var mPathEffectBitmap: Bitmap? = null
    private var mImageAssets: ImageAssets = ImageAssets()
    private var mBrushBounds = 50

    //滤镜 drawBitmap基础白色笔刷换颜色
    var colorMatrix = ColorMatrix(
        floatArrayOf(
            1f, 1f, 1f, 1f, 1f,
            1f, 1f, 1f, 1f, 1f,
            1f, 1f, 1f, 1f, 1f,
            1f, 1f, 1f, 1f, 1f
        )
    )
    private var mMode = Mode.DRAW
    private var mTouchStatus = TouchStatus.UP
    private var mPathStyle = PathStyle.SMOOTH

    enum class Mode {
        DRAW, ERASER, PATH_EFFECT
    }

    enum class TouchStatus {
        UP, MOVE, DOWN
    }

    enum class PathStyle {
        NORMAL, SMOOTH, SPACE, CUSTOM
    }

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    interface Callback {
        fun onUndoRedoStatusChanged()
    }

    fun setCallback(callback: Callback?) {
        mCallback = callback
    }

    private fun init() {
        isDrawingCacheEnabled = true

        penSize = 20
        eraserSize = 20
        mXferModeDraw = PorterDuffXfermode(PorterDuff.Mode.SRC)
        mXferModeClear = PorterDuffXfermode(PorterDuff.Mode.CLEAR)

        mDrawPaint.apply {
            style = Paint.Style.STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
//            isFilterBitmap = true
            strokeWidth = penSize.toFloat()
            color = Color.WHITE
            xfermode = mXferModeDraw
        }

        mEraserPaint.apply {
            style = Paint.Style.STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = eraserSize.toFloat()
//            color = Color.TRANSPARENT
            xfermode = mXferModeClear
        }

        mPathPaint.apply {
//            color = Color.TRANSPARENT
            style = Paint.Style.STROKE
            isAntiAlias = true
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = penSize.toFloat()
            isFilterBitmap = true
//            colorFilter = ColorMatrixColorFilter(colorMatrix)
            xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
        }

        mImageAssets.buildImages(
            BitmapFactory.decodeResource(
                resources,
                R.drawable.bifeng
            )
        )
    }

    fun initDrawList(list: MutableList<DrawingInfo>?) {
        list?.let { mutableList ->
            mDrawingList = mutableListOf()
            mDrawingList?.clear()
            mutableList.forEach {
                mDrawingList?.add(it)
            }
            reDraw()
        } ?: clear()
        mRemovedList?.clear()
    }

    private fun initBuffer() {
        mBufferBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mBufferBitmap!!.eraseColor(Color.TRANSPARENT)
        mBufferCanvas = Canvas(mBufferBitmap!!)

        mMovingBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mMovingBitmap!!.eraseColor(Color.TRANSPARENT)
        mMovingCanvas = Canvas(mMovingBitmap!!)
        initPathEffectBitmap()
    }

    private fun initPathEffectBitmap() {
        mPathEffectBufferBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mPathEffectBufferBitmap!!.eraseColor(Color.TRANSPARENT)
        mPathEffectBufferCanvas = Canvas(mPathEffectBufferBitmap!!)
    }

    abstract class DrawingInfo {
        var paint: Paint? = null
        abstract fun draw(canvas: Canvas?)
    }

    private class PathDrawingInfo : DrawingInfo() {
        var path: Path? = null
        override fun draw(canvas: Canvas?) {
            canvas!!.drawPath(path!!, paint!!)
        }
    }

    private class BitmapDrawingInfo : DrawingInfo() {
        var buffer: Bitmap? = null

        fun setBufferBitmap(bitmap: Bitmap?) {
            bitmap?.let {
                buffer = Bitmap.createBitmap(it)
            }
        }

        override fun draw(canvas: Canvas?) {
            buffer?.let {
                canvas?.drawBitmap(it, 0f, 0f, paint)
            }
        }

    }

    class ImageAssets {
        //修改更新后
        private var images = mutableListOf<Bitmap>()

        //原图
        private var mBitmaps = mutableListOf<Bitmap>()

        private var mBoundRatio = 1f

        //是否随机旋转
        private var mShowTextureRandom = 1
        //顺序渲染位置
        private var mPosition = 0

        fun isShowTextureRandom(showTextureRandom: Int){
            mShowTextureRandom = showTextureRandom
        }

        /**
         * 通过旋转构建数据
         */
        fun buildRotateImages(drawable: Int, res: Resources, bounds: Int = 0) {
            mBoundRatio = 1f
            mBitmaps.clear()
            images.clear()
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    for (i in 0..12) {
                        val bitmap = BitmapFactory.decodeResource(
                            res,
                            drawable
                        )
                        val rotateBitmap = BitmapUtils.rotateBitmap(i * 30, bitmap)
                        mBitmaps.add(rotateBitmap)
                        images.add(
                            if (bounds > 0) BitmapUtil.scaleBitmap(
                                rotateBitmap,
                                bounds
                            ) else rotateBitmap
                        )
                    }
                }
            }
        }

        fun initPosition(){
            mPosition = 0
        }

        fun buildImages(bitmap: Bitmap, bounds: Int = 0) {
            mBoundRatio = 1f
            mBitmaps.clear()
            images.clear()
            mBitmaps.add(bitmap)
            images.add(if (bounds > 0) BitmapUtil.scaleBitmap(bitmap, bounds) else bitmap)
        }

        fun buildImages(assetsPath: List<String>, bounds: Int = 0){
            mBoundRatio = 1.414f
            mBitmaps.clear()
            images.clear()
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    for (i in 0..assetsPath.lastIndex) {
                        val bitmap = BitmapFactory.decodeFile(assetsPath[i])
                        mBitmaps.add(bitmap)
                        images.add(
                            if (bounds > 0) BitmapUtil.scaleBitmap(
                                bitmap,
                                (bounds * mBoundRatio).toInt()
                            ) else bitmap
                        )
                    }
                }
                withContext(Dispatchers.Main){
                    Log.e("images", images.toString())
                }
            }
        }

        /**
         * 随机获取资源
         */
        fun randomAssets(): Bitmap {
            return try {
                var position = 0
                if(mShowTextureRandom == 1) {
                    position = (0..images.lastIndex).random()
                }else{
                    mPosition++
                    if(mPosition == images.lastIndex){
                        mPosition = 0
                    }
                    position = mPosition
                }
                images[position]
            }catch (e: Exception){
                Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
            }
        }

        fun updateBitmapBounds(size: Int) {
            val resize = if(size < 10) 10 else size
            GlobalScope.launch {
                withContext(Dispatchers.IO) {
                    for (i in 0..mBitmaps.lastIndex) {
                        images[i] = BitmapUtil.scaleBitmap(
                            mBitmaps[i],
                            (resize * mBoundRatio).toInt()
                        )
                    }
                }
            }
        }
    }

    var mode: Mode
        get() = mMode
        set(mode) {
            if (mode != mMode) {
                mMode = mode
            }
        }

    /**
     * 设置笔刷大小
     */
    fun setPenRawSize(size: Int) {
        val newSize = if(size < 10) 10 else size
        if (mMode == Mode.DRAW) {
            penSize = newSize
            mDrawPaint.strokeWidth = penSize.toFloat()
        }
        if (mMode == Mode.ERASER) {
            eraserSize = newSize
            mEraserPaint.strokeWidth = eraserSize.toFloat()
        }
        if (mMode == Mode.PATH_EFFECT) {
            penSize = newSize
            mBrushBounds = newSize

            mImageAssets.updateBitmapBounds(mBrushBounds)
        }
    }

    /**
     * 是否是虚线
     */
    fun isDashPath(b: Boolean) {
        if (b) {
            mode = Mode.DRAW
            val path = Path()
            path.addRoundRect(
                0f,
                0f,
                penSize.toFloat().times(2),
                penSize.toFloat(),
                floatArrayOf(
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(
                        2
                    ),
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(2),
                    penSize.toFloat().div(2)
                ),
                Path.Direction.CCW
            )
            //圆点虚线
            mDrawPaint.pathEffect = PathDashPathEffect(
                path,
                penSize.toFloat().times(3),
                0f,
                PathDashPathEffect.Style.ROTATE
            )
        } else {
            mDrawPaint.pathEffect = null
        }
    }

    /**
     * 设置笔触样式
     */
    fun setPenStyle(drawable: Int, isRotate: Boolean = false, size: Int = 0) {
        mode = Mode.PATH_EFFECT
        mPathStyle = PathStyle.SMOOTH
        if (size > 0) {
            mBrushBounds = size
        } else {
            mBrushBounds = penSize
        }
        if (isRotate) {
            mImageAssets.buildRotateImages(
                drawable, resources, mBrushBounds
            )
        } else {
            mImageAssets.buildImages(
                BitmapFactory.decodeResource(
                    resources,
                    drawable
                ), mBrushBounds
            )
        }
    }

    /**
     * 设置素材笔刷样式
     */
    fun setPenStyle(assetsPath: MutableList<String>, size: Int = 0, showTextureRandom: Int = 1){
        mode = Mode.PATH_EFFECT
        mPathStyle = PathStyle.SPACE
        if (size > 0) {
            mBrushBounds = size
        } else {
            mBrushBounds = penSize
        }
        mImageAssets.isShowTextureRandom(showTextureRandom)
        mImageAssets.buildImages(assetsPath, mBrushBounds)
    }

    /**
     * 设置笔触路径风格
     */
    fun setPathStyle(pathStyle: PathStyle) {
        mPathStyle = pathStyle
    }

    private fun reDraw() {
        if (mDrawingList != null) {
//            mBufferCanvas?.drawPaint(mEraserPaint)
//            mPathEffectBufferCanvas?.drawPaint(mEraserPaint)

            mBufferBitmap!!.eraseColor(Color.TRANSPARENT)
            mPathEffectBufferBitmap!!.eraseColor(Color.TRANSPARENT)
            mMovingBitmap!!.eraseColor(Color.TRANSPARENT)

            for (drawingInfo in mDrawingList!!) {
                if (drawingInfo is PathDrawingInfo) {
                    drawingInfo.draw(mBufferCanvas)
                } else {
                    drawingInfo.draw(mPathEffectBufferCanvas)
                }
                drawingInfo.draw(mMovingCanvas)
            }
            invalidate()
        }
    }

    var penColor: Int
        get() = mDrawPaint.color
        set(color) {
            mDrawPaint.color = color

            val red = color and 0xff0000 shr 16
            val green = color and 0x00ff00 shr 8
            val blue = color and 0x0000ff
            colorMatrix = ColorMatrix(transferColorMatrix(red, green, blue))
            mPathPaint.colorFilter = ColorMatrixColorFilter(colorMatrix)
        }
    var penAlpha: Int
        get() = mPenAlpha
        set(alpha) {
            mPenAlpha = alpha
            if (mMode == Mode.DRAW) {
                mDrawPaint.alpha = alpha
            }
        }

    fun hasColorMatrix(isColorMatrix: Boolean){
        if(isColorMatrix){
            mPathPaint.colorFilter = ColorMatrixColorFilter(colorMatrix)
        }else{
            mPathPaint.colorFilter = null
        }
    }

    fun canRedo(): Boolean {
        return mRemovedList != null && mRemovedList!!.size > 0
    }

    fun canUndo(): Boolean {
        return mDrawingList != null && mDrawingList!!.size > 0
    }

    fun redo() {
        val size = if (mRemovedList == null) 0 else mRemovedList!!.size
        if (size > 0) {
            val info = mRemovedList!!.removeAt(size - 1)
            mDrawingList!!.add(info)
            mCanEraser = true
            reDraw()
            if (mCallback != null) {
                mCallback!!.onUndoRedoStatusChanged()
            }
        }
    }

    fun undo() {
        val size = if (mDrawingList == null) 0 else mDrawingList!!.size
        if (size > 0) {
            val info = mDrawingList!!.removeAt(size - 1)
            if (mRemovedList == null) {
                mRemovedList = ArrayList(MAX_CACHE_STEP)
            }
            if (size == 1) {
                mCanEraser = false
            }
            mRemovedList!!.add(info)
            reDraw()
            if (mCallback != null) {
                mCallback!!.onUndoRedoStatusChanged()
            }
        }
    }

    /**
     * 设置阴影
     */
    fun setShadowLayer(radius: Float, color: Int = Color.BLACK) {
        mDrawPaint.setShadowLayer(radius, 0f, 0f, color)
    }

    fun clear() {
        if (mBufferBitmap != null) {
            if (mDrawingList != null) {
                mDrawingList!!.clear()
            }
            if (mRemovedList != null) {
                mRemovedList!!.clear()
            }
            mCanEraser = false
            mBufferBitmap!!.eraseColor(Color.TRANSPARENT)
            mPathEffectBufferBitmap!!.eraseColor(Color.TRANSPARENT)
            mMovingBitmap!!.eraseColor(Color.TRANSPARENT)
            invalidate()
            if (mCallback != null) {
                mCallback!!.onUndoRedoStatusChanged()
            }
        }
    }

    fun addTouchAction(action: DrawView.TouchAction) {
        mTouchAction = action
    }

    fun buildBitmap(): Bitmap? {
        return try {
            val bm = drawingCache
            val result = Bitmap.createBitmap(bm)
            destroyDrawingCache()
            result
        } catch (e: Exception) {
            null
        }
    }

    private fun saveDrawingPath() {
        if (mDrawingList == null) {
            mDrawingList = ArrayList(MAX_CACHE_STEP)
        } else if (mDrawingList!!.size == MAX_CACHE_STEP) {
            mDrawingList!!.removeAt(0)
        }

        val cachePaint = Paint(
            when (mode) {
                Mode.DRAW -> mDrawPaint
                Mode.ERASER -> mEraserPaint
                Mode.PATH_EFFECT -> mPathPaint
            }
        )

        if (mode == Mode.PATH_EFFECT) {
            val info = BitmapDrawingInfo()
            info.paint = cachePaint
            info.setBufferBitmap(mPathEffectBufferBitmap)
            mDrawingList!!.add(info)
        } else {
            val cachePath = Path(mPath)

            val info = PathDrawingInfo()
            info.path = cachePath
            info.paint = cachePaint
            mDrawingList!!.add(info)
        }

        mMovingBitmap?.eraseColor(Color.TRANSPARENT)
        mDrawingList?.forEach {
            it.draw(mMovingCanvas)
        }

        mCanEraser = true
        if (mCallback != null) {
            mCallback!!.onUndoRedoStatusChanged()
        }
    }

    /**
     * 通过rgb转换4*5颜色矩阵
     */
    private fun transferColorMatrix(red: Int, green: Int, blue: Int): FloatArray {
        val R = red.toFloat() / 255
        val G = green.toFloat() / 255
        val B = blue.toFloat() / 255
        val matrixItems = floatArrayOf(
            R, 0f, 0f, 0f, 0f,
            0f, G, 0f, 0f, 0f,
            0f, 0f, B, 0f, 0f,
            0f, 0f, 0f, 1f, 0f,
            0f, 0f, 0f, 0f, 1f
        )
        return matrixItems
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        if (mMovingBitmap != null) {
            canvas.drawBitmap(mMovingBitmap!!, 0f, 0f, null)
        }

        if (mPath != null && mTouchStatus == TouchStatus.MOVE) {
            when (mode) {
                Mode.DRAW -> {
                    canvas.drawPath(mPath!!, mDrawPaint)
                }
                Mode.ERASER -> {
                    canvas.drawPath(mPath!!, mEraserPaint)
                }
                Mode.PATH_EFFECT -> {
                    mPathEffectBufferBitmap?.let {
                        canvas.drawBitmap(it, 0f, 0f, mPathPaint)
                    }
                }
            }
        }
    }

    /**
     * 计算旋转角度
     */
    fun getAngle(x1: Float,y1: Float,x2: Float,y2: Float): Float{
        val Xpoint = Point(x1.toInt()+100, y1.toInt())
        val a = x2 - x1
        val b = y2-y1
        val c = Xpoint.x-x1
        val d = Xpoint.y-y1

        val mole = (a*c) +(b*d)
        val sqrtAB = sqrt(a*a + b*b)
        val aqrtCD = sqrt(c*c + d*d)

        var rads = acos(mole/(sqrtAB * aqrtCD))
        if(y1 > y2){
            rads = -rads
        }
        Log.e("getAngle", rads.toString())

        return (rads * 180/PI).toFloat()
    }

    /**
     * 设置画笔模糊度
     */
    fun setShadowBlend(progress: Int, style: BlurMaskFilter.Blur = BlurMaskFilter.Blur.NORMAL) {
        try {
            if (progress > 0) {
                val filter = BlurMaskFilter(progress.toFloat() / 5, style)
                mMaskFilter = filter
                mDrawPaint.maskFilter = filter
            } else {
                mDrawPaint.maskFilter = null
            }
        } catch (e: Exception) {

        }
    }

    private var mLastPoint:Point = Point()

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return false
        }
        val action = event.action and MotionEvent.ACTION_MASK
        val x = event.x
        val y = event.y

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                mTouchStatus = TouchStatus.DOWN
                mImageAssets.initPosition()
                mLastPoint = Point(x.toInt(),y.toInt())
                mLastX = x
                mLastY = y
                mLastMoveX = x
                mLastMoveY = y
                if (mPath == null) {
                    mPath = Path()
                }
                mPath!!.moveTo(x, y)
                //避免模糊笔触重叠
                initPathEffectBitmap()
            }
            MotionEvent.ACTION_MOVE -> {
                mTouchStatus = TouchStatus.MOVE
                //这里终点设为两点的中心点的目的在于使绘制的曲线更平滑，如果终点直接设置为x,y，效果和lineto是一样的,实际是折线效果
                mPath!!.quadTo(mLastX, mLastY, (x + mLastX) / 2, (y + mLastY) / 2)
                if (mBufferBitmap == null) {
                    initBuffer()
                }
                if (mMode == Mode.ERASER && !mCanEraser) {
                    return false
                }
                when (mMode) {
                    Mode.DRAW -> {
                        mBufferCanvas!!.drawPath(mPath!!, mDrawPaint)
                    }
                    Mode.ERASER -> {
                        mBufferCanvas!!.drawPath(mPath!!, mEraserPaint)
                    }
                    Mode.PATH_EFFECT -> {
                        val fLen = sqrt(
                            (x - mLastMoveX).toDouble().pow(2.toDouble())
                                    + (y - mLastMoveY).toDouble().pow(2.toDouble())
                        )
                        if (fLen > 1) {
                            val fromPoint = Point(mLastMoveX.toInt(), mLastMoveY.toInt())
                            val controlPoint =
                                Point((x + mLastMoveX).toInt() / 2, (y + mLastMoveY).toInt() / 2)
                            val toPoint = Point(x.toInt(), y.toInt())
                            //使用牛顿切线法计算曲线公式
                            val pointList = MFBezierCurvesTool.pointsWithFrom(
                                fromPoint,
                                toPoint,
                                controlPoint,
                                20f
                            )

                            when (mPathStyle) {
                                //平滑
                                PathStyle.SMOOTH -> {
                                    for (i in 0..pointList.lastIndex) {
                                        mPathEffectBufferCanvas!!.drawBitmap(
                                            mImageAssets.randomAssets(),
                                            pointList[i].x.toFloat(),
                                            pointList[i].y.toFloat(),
                                            mPathPaint
                                        )
                                    }
                                }
                                //空隙
                                PathStyle.SPACE -> {
                                    val ratio = 1.5
                                    val space = (mBrushBounds * ratio).toInt()
                                    if (pointList.size < space) return false
                                    for (i in 0..pointList.lastIndex) {
                                        if (i % space == 0 && i / space < pointList.lastIndex / space &&
                                            mLastMoveX != pointList[i].x.toFloat() && mLastMoveY != pointList[i].y.toFloat()
                                        ) {

                                            val matrix = Matrix()
                                            matrix.postTranslate(
                                                pointList[i].x.toFloat(),
                                                pointList[i].y.toFloat()
                                            )

                                            matrix.preRotate(getAngle(mLastPoint.x.toFloat() ,mLastPoint.y.toFloat() ,pointList[i].x.toFloat(),pointList[i].y.toFloat()).toFloat())
                                            mPathEffectBufferCanvas!!.drawBitmap(
                                                mImageAssets.randomAssets(),
                                                matrix,
                                                mPathPaint
                                            )
                                            mLastPoint = Point(pointList[i].x, pointList[i].y)
                                            if (i / space == (pointList.lastIndex / space) - 1) {
                                                mLastMoveX = pointList[i].x.toFloat()
                                                mLastMoveY = pointList[i].y.toFloat()
                                                return false
                                            }
                                        }
                                    }
                                }
                                //自定义
                                PathStyle.CUSTOM -> {
                                    val coustomBrushBounds = mBrushBounds / 2
                                    if (pointList.size < coustomBrushBounds) return false
                                    for (i in 0..pointList.lastIndex) {
                                        if (i % coustomBrushBounds == 0 && i / coustomBrushBounds < pointList.lastIndex / coustomBrushBounds) {
                                            mPathEffectBufferCanvas!!.drawBitmap(
                                                mImageAssets.randomAssets(),
                                                pointList[i].x.toFloat(),
                                                pointList[i].y.toFloat(),
                                                mPathPaint
                                            )
                                            if (i / coustomBrushBounds == (pointList.lastIndex / coustomBrushBounds) - 1) {
                                                mLastMoveX = pointList[i].x.toFloat()
                                                mLastMoveY = pointList[i].y.toFloat()
                                                return false
                                            }
                                        }
                                    }
                                }
                            }


                            mLastMoveX = x
                            mLastMoveY = y
                        }
                    }
                }
                invalidate()
                mLastX = x
                mLastY = y
            }
            MotionEvent.ACTION_UP -> {
                mTouchStatus = TouchStatus.UP
                if (mMode == Mode.DRAW || mMode == Mode.PATH_EFFECT || mCanEraser) {
                    saveDrawingPath()
                }
                mPath!!.reset()
                mTouchAction?.event_up()
            }
        }
        return true
    }

    private var mBounds: Point? = null
    fun setBounds(bounds: Point) {
        mBounds = bounds
    }

    companion object {
        private const val MAX_CACHE_STEP = 1000
    }
}