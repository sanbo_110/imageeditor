package com.tuyue.common_sdk.model

import com.chad.library.adapter.base.entity.MultiItemEntity


/**
 * Create by guojian on 2021/7/18
 * Describe：
 **/
class DoodleColorModel(override val itemType: Int, c: Int = 0) : MultiItemEntity {
    var color: Int = c
}