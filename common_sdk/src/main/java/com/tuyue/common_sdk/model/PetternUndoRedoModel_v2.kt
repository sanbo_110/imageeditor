package com.tuyue.common_sdk.model

import android.graphics.Bitmap
import com.divyanshu.draw.widget.MyPath
import com.divyanshu.draw.widget.PaintOptions
import com.tuyue.common_sdk.image_edit.GPUImageModel
import com.tuyue.common_sdk.image_edit.PetternType
import com.tuyue.common_sdk.tools.GPUImageFilterTools
import com.tuyue.core.util.CheckUtils
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import java.util.LinkedHashMap


/**
 * Create by guojian on 2021/4/8
 * Describe：撤销/重置数据类bi
 **/
class PetternUndoRedoModel_v2 (
    //一级菜单类型
    var type: PetternType,
    //滤镜
    var filter: MutableList<GPUImageModel> = mutableListOf(),
    //原图
    var bitmap: Bitmap? = null,
    //边框
    var frameAssetsModel: FrameAssetsModel? = null,
    //笔刷paths
    var paths: Bitmap? = null,
    // 记录一级菜单选中位置
    var positionMaps: HashMap<PetternType, SelectRecordModel> = hashMapOf(),
        ){

    fun getGPUImageFilterGroup(): GPUImageFilter{
        return if(CheckUtils.isEmpty(filter)){
            GPUImageFilter()
        }else{
            val gpuimageFilterGroup = GPUImageFilterGroup()
            filter.forEach {
                gpuimageFilterGroup.addFilter(it.filter)
            }
            gpuimageFilterGroup
        }
    }
}

/**
 * 克隆对象
 */
fun PetternUndoRedoModel_v2.clone() :PetternUndoRedoModel_v2 {
    val model = this
    val cloneModel = PetternUndoRedoModel_v2(model.type)
    val cloneFilter: MutableList<GPUImageModel> = mutableListOf()
    model.filter.forEach {
        val cloneGPUImageModel = GPUImageModel()
        cloneGPUImageModel.apply {
            this.filter = GPUImageFilterTools.createFilter(it.filter, it.progressState?.floatArray)
            this.type = it.type
            this.filterId = it.filterId
            this.hasProgress = it.hasProgress
            it.progressState?.let {
                val cloneProgressState = FilterProgressState(it.progress, it.floatArray)
                this.progressState = cloneProgressState
            }
            this.position = it.position
        }
        cloneFilter.add(cloneGPUImageModel)
    }
    val clonePositionMaps: HashMap<PetternType, SelectRecordModel> = hashMapOf()
    model.positionMaps.forEach {
        clonePositionMaps[it.key] = it.value
    }
    cloneModel.apply {
        this.type = model.type
        this.filter = cloneFilter
        this.bitmap = model.bitmap
        this.frameAssetsModel = model.frameAssetsModel
        this.paths = model.paths
        this.positionMaps = clonePositionMaps
    }
    return cloneModel
}


