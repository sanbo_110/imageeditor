package com.tuyue.common_sdk.model

data class FilterProgressState(
    var progress: Int,
    var floatArray: FloatArray
)
