package com.tuyue.common_sdk.model


/**
 * Create by guojian on 2021/8/6
 * Describe：
 **/
class MessageEvent(val code: Int, val event: Any? = null )

const val EVENT_REFRESH_PAY_RESULT = 10