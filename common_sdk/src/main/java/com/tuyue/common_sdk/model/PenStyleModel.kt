package com.tuyue.common_sdk.model

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.tuyue.common_sdk.widget.PaletteView


/**
 * Create by guojian on 2021/7/17
 * Describe：
 **/
class PenStyleModel(override val itemType: Int) : MultiItemEntity {
    var assetsStyle: AssetsStyle = AssetsStyle.NATIVE
    var penStyle: PenStyle = PenStyle.BITMAP
    var pathStyle: PaletteView.PathStyle = PaletteView.PathStyle.SMOOTH

    //笔触本地资源
    var drawables: Array<Int>? = null
    var icon: Int? = null

    //笔触id
    var brushTextureID: Int = 0

    var brushTextureTitle: String? = null
    var brushTextureUpdateTime: Long = 0
    var brushTextureZipMd5: String? = null
    var brushTextureIcon: String? = null
    var brushTextureZip: String = ""
    var brushTextureType: String? = null

    //笔触本地路径
    var paths: MutableList<String>? = null

    //渲染时是否可以随机旋转角度
    var isRotate: Boolean = false

    //是否随机展示素材笔刷，0不随机，1为随机
    var showTextureRandom: Int = 1
    //是否为会员资源 值为0 不是， 值为1是
    var vipResources: Int = 0
}
enum class PenStyle{
    DRAW, BITMAP,
}
enum class AssetsStyle{
    NATIVE, NETWORK
}