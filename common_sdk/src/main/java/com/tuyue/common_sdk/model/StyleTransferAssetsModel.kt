package com.tuyue.common_sdk.model

import com.tuyue.common_sdk.image_edit.PetternType
import com.tuyue.common_sdk.widget.SecondNode
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter


/**
 * Create by guojian on 2021/3/2
 * Describe：
 **/
class StyleTransferAssetsModel (
    type: PetternType, 
    var styletransferID: Int = 0,
    var styletransferTime: Long = 0,
    var styletransferTitle: String? = null,
    var styletransferIcon: String? = null,
    var styletransferZip: String? = null,
    var styletransferZipMd5: String? = null,
    var styletransferBlendingRatio: Float = 0f,
    
    var path: String = "", 
    var styleIcon: Int = 0,
    var textureFilter: GPUImageFilter? = null, 
    var iconUrl: String = ""
    , var vipResources: Int = 0
        )
    : SecondNode(styletransferTitle?:"", textureFilter, styleIcon, type)