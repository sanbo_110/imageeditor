package com.tuyue.common_sdk.model

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.tuyue.core.resbody.VipResModel


/**
 * Create by guojian on 2021/8/2
 * Describe：
 **/
class VipPriceModel(
    override val itemType: Int,
    var title: String? = null,
    var data: VipResModel? = null
)  : MultiItemEntity