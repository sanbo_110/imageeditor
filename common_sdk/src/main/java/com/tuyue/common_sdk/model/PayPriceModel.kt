package com.tuyue.common_sdk.model

import com.chad.library.adapter.base.entity.MultiItemEntity


/**
 * Create by guojian on 2021/8/2
 * Describe：
 **/
class PayPriceModel(
    override val itemType: Int,
    var title: String? = null,
    var icon: Int = 0,
    var payMethod: String? = null
)  : MultiItemEntity