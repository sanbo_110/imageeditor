package com.tuyue.common_sdk.image_edit

import com.tuyue.common_sdk.model.FilterProgressState
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter

data class GPUImageModel(
    //调整，滤镜，纹理
    var filter: GPUImageFilter? = null,
    var type: PetternType = PetternType.NORMAL,
    var filterId: String = "",
    var position: Int = 0,
    //GPUImageFilter的状态：进度，参数（根据GPUImageFilter子类不同，用float数组记录）
    var progressState: FilterProgressState? = null,
    //是否支持进度条
    var hasProgress: Boolean = true,
)
