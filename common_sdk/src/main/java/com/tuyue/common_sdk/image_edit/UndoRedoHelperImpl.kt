package com.tuyue.common_sdk.image_edit

import android.graphics.Bitmap
import android.text.TextUtils
import android.util.Log
import com.tuyue.common_sdk.model.FilterProgressState
import com.tuyue.common_sdk.model.FrameAssetsModel
import com.tuyue.common_sdk.model.PetternUndoRedoModel
import com.tuyue.common_sdk.model.SelectRecordModel
import com.tuyue.common_sdk.tools.GPUImageFilterTools
import com.tuyue.core.util.CheckUtils
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import java.util.*
import kotlin.collections.HashMap

/**
 * create by guojian
 * Date: 2020/12/31
 * 多管道滤镜帮助类实现类
 * @see [mImageFilterGroup] 操作滤镜栈的管理(包括撤销后的数据)
 * @see [mUndoFilterGroup] 重置撤销滤镜栈的管理
 *
 * 当调用 [undoFilter] 撤销滤镜时，[mImageFilterGroup] 栈中执行出栈操作，[mUndoFilterGroup] 进栈。
 * 当调用 [redoFilter] 重置一次撤销滤镜时，[mUndoFilterGroup] 栈中执行出栈操作，[mImageFilterGroup] 进栈。
 */
class UndoRedoHelperImpl : IFilterGroupHelper {

    /**
     * 当前操作队列
     */
    private var mCurrentQueue: PetternUndoRedoModel = PetternUndoRedoModel(PetternType.NORMAL)

    /**
     * 临时记录一次滤镜状态，用于还原操作
     */
    private val mRecordFilterGroups: LinkedList<PetternUndoRedoModel> = LinkedList()

    /**
     * 保存撤销重置
     */
    private val mUndoRecordFilterGroups: LinkedList<PetternUndoRedoModel> = LinkedList()

    /**
     * 当前操作状态
     */
    private var mPetternType: PetternType = PetternType.NORMAL

    private var mFilterGroup = mutableListOf<GPUImageFilter>()
    private var mAdjustGroup = mutableListOf<GPUImageFilter>()
    private var mTextureGroup = mutableListOf<GPUImageFilter>()
    private val mLastFilterState : HashMap<String, HashSet<GPUImageFilter>> = hashMapOf()
    //保存当前操作状态进度条
    private var mProgressQueue = hashMapOf<String, FilterProgressState>()
    //进度条撤销队列
    private var mUndoProgressQueue : LinkedList<HashMap<String, FilterProgressState>> = LinkedList()
    //进度条反撤销队列
    private var mRedoProgressQueue : LinkedList<HashMap<String, FilterProgressState>> = LinkedList()

    private var mPositionQueue = hashMapOf<PetternType, SelectRecordModel>()
    private var mCurrentPositions = hashMapOf<PetternType, SelectRecordModel>()

    override fun addFilter(filter: GPUImageFilter, petternType: PetternType, position: Int?, hasProgress: Boolean) {
        //滤镜和纹理只保留单管道
        position?.let {
            val model = SelectRecordModel(it, hasProgress)
            mCurrentPositions.put(petternType, model)
        }
        val filterSet = when (petternType) {
            PetternType.FILTER -> {
                mFilterGroup
            }
            PetternType.ADJUST -> {
                mAdjustGroup
            }
            PetternType.TEXTURE -> {
                mTextureGroup
            }
            else -> {
                mFilterGroup
            }
        }
        if (petternType == PetternType.FILTER || petternType == PetternType.TEXTURE) {
            filterSet.clear()
        }
        //去重复滤镜

        if(contains(filterSet, filter)) {
            for (i in 0..filterSet.lastIndex) {
                if (TextUtils.equals(
                        filterSet[i].javaClass.simpleName,
                        filter.javaClass.simpleName
                    )
                ) {
                    filterSet.add(i, filter)
                    filterSet.removeAt(i + 1)
                    break
                }
            }
        }else {
            filterSet.add(filter)
        }
        Log.e("mLastFilterState", mLastFilterState.toString())
        mCurrentQueue.type = petternType
        mCurrentQueue.filter.addFilter(filter)
        mCurrentQueue.filter.updateMergedFilters()
    }

    private fun contains(mutableList: MutableList<GPUImageFilter>, filter: GPUImageFilter): Boolean{
        for (i in 0..mutableList.lastIndex){
            if(TextUtils.equals(mutableList[i].javaClass.simpleName, filter.javaClass.simpleName)){
                return true
            }
        }
        return false
    }

    fun getLastPositionByPetternType(type: PetternType): SelectRecordModel?{
        return mPositionQueue[type]
    }

    /**
     * 记录进度条状态
     */
    fun recordProgress(filter: GPUImageFilter, progress: Int, floatArray: FloatArray){
        val filterProgressState = FilterProgressState(progress, floatArray)
        mProgressQueue[filter.javaClass.simpleName] = filterProgressState
        Log.e("mUndoProgressQueue", mUndoProgressQueue.toString())
    }

    /**
     * 获取当前操作状态保存的进度条记录
     */
    fun getFilterProgress(filter: GPUImageFilter): Int?{
        var position : Int? = null
        if(mProgressQueue.size > 0){
            position = mProgressQueue[filter.javaClass.simpleName]?.progress
        }
        if(position != null) return position
        return try {
            mUndoProgressQueue.last[filter.javaClass.simpleName]?.progress
        }catch (e: Exception){
            null
        }
    }

    fun getSelectFilter(filter: GPUImageFilter): GPUImageFilter?{
        getFilterGroup().filters?.forEach {
            if(TextUtils.equals(it.javaClass.simpleName, filter.javaClass.simpleName)){
                return it
            }
        }
        return null
    }

    /**
     * 记录上一次所有filter状态
     */
    fun recordLastFilterState(){
        if(mUndoProgressQueue.size==0)return
        mLastFilterState.clear()
        val lastProgressModel = mUndoProgressQueue.last
        val filterGroup = hashSetOf<GPUImageFilter>()
        mFilterGroup.forEach {gpuimagefilter->
            val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
            floatArray?.let {

                filterGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
            }
        }
        mLastFilterState[PetternType.FILTER.name] = filterGroup

        val adjustGroup = hashSetOf<GPUImageFilter>()
        mAdjustGroup.forEach {gpuimagefilter->
            val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
            floatArray?.let {
                adjustGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
            }
        }
        mLastFilterState[PetternType.ADJUST.name] = adjustGroup

        val textureGroup = hashSetOf<GPUImageFilter>()
        mTextureGroup.forEach {gpuimagefilter->
            val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
            floatArray?.let {
                textureGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
            }
        }
        mLastFilterState[PetternType.TEXTURE.name] = textureGroup
        Log.e("recordLastFilterState", mLastFilterState.toString())
    }

    /**
     * 还原上一次确认操作滤镜状态
     */
    fun resetFilterState(){
        if(mUndoProgressQueue.size==0) {
            clearAllFilter()
            return
        }
        val lastProgressModel: HashMap<String, FilterProgressState> = hashMapOf()
        mUndoProgressQueue.last.forEach {
            lastProgressModel[it.key] = FilterProgressState(it.value.progress, it.value.floatArray)
        }
        mLastFilterState[PetternType.FILTER.name]?.let { hashSet ->
            mFilterGroup = mutableListOf()
            hashSet.forEach {gpuimagefilter->
                val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
                floatArray?.let {
                    mFilterGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
                }
            }
        }

        mLastFilterState[PetternType.ADJUST.name]?.let { hashSet ->
            mAdjustGroup = mutableListOf()
            hashSet.forEach {gpuimagefilter->
                val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
                floatArray?.let {
                    mAdjustGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
                }
            }
        }

        mLastFilterState[PetternType.TEXTURE.name]?.let { hashSet ->
            mTextureGroup = mutableListOf()
            hashSet.forEach {gpuimagefilter->
                val floatArray = lastProgressModel[gpuimagefilter.javaClass.simpleName]?.floatArray
                floatArray?.let {
                    mTextureGroup.add(GPUImageFilterTools.createFilter(gpuimagefilter, it))
                }
            }
        }
        Log.e("resetFilterState", mLastFilterState.toString())
    }

    override fun getFilterGroup(): GPUImageFilterGroup {
        val filterGroup = GPUImageFilterGroup()
        mFilterGroup.forEach {
            filterGroup.addFilter(it)
        }
        mAdjustGroup.forEach {
            filterGroup.addFilter(it)
        }
        mTextureGroup.forEach {
            filterGroup.addFilter(it)
        }
        if (CheckUtils.isEmpty(filterGroup)) {
            return filterGroup
        }
        return filterGroup
    }

    /**
     * 重置滤镜
     * @param petternType 类型
     */
    override fun clearFilterByType(petternType: PetternType) {
        when (petternType) {
            PetternType.FILTER -> {
                mFilterGroup.clear()
            }
            PetternType.ADJUST -> {
                mAdjustGroup.clear()
            }
            PetternType.TEXTURE -> {
                mTextureGroup.clear()
            }
            else -> {
                return
            }
        }
    }

    /**
     * 清除所有滤镜
     */
    fun clearAllFilter(){
        mFilterGroup.clear()
        mAdjustGroup.clear()
        mTextureGroup.clear()
        mPositionQueue.clear()
    }

    fun clearUndoRedoStack(){
        mRecordFilterGroups.clear()
        mUndoRecordFilterGroups.clear()
    }

    /**
     * 取消一次操作
     */
    fun removeLast(){
        if(mRecordFilterGroups.size > 0){
            mRecordFilterGroups.removeLast()
        }
    }

    /**
     * 撤销滤镜
     * @return 返回是否可以渲染滤镜的状态，false 不可渲染重制视图
     */
    override fun undoFilter(petternType: PetternType): Boolean {
        resetLastFilterState()
        return isResetFilter()
    }

    /**
     * 重置一次撤销
     */
    override fun redoFilter(petternType: PetternType) {
        redoResetLastFilterState()
    }

    /**
     * 是否可以重置
     */
    override fun isRedoResetFilter(petternType: PetternType): Boolean {
        return mUndoRecordFilterGroups.isNotEmpty()
    }

    override fun isResetFilter(petternType: PetternType): Boolean {
        return mRecordFilterGroups.size > 0
    }

    /**
     * 修改队列中最后一个元素参数
     */
    fun updateLastRecord(filter: GPUImageFilterGroup, bitmap: Bitmap){
        if(mRecordFilterGroups.isEmpty())return
        mRecordFilterGroups.last.filter = filter
        mRecordFilterGroups.last.bitmap = bitmap
        //替换原图后清除记录的位置信息
        mPositionQueue.clear()
    }

    /**
     * 记录撤销，将数据加入操作序列
     */
    override fun recordFilterState(petternType: PetternType, bitmap: Bitmap?, frameAssetsModel: FrameAssetsModel?, drawBitmap: Bitmap?) {
        mPetternType = petternType
        val filterGroup = getFilterGroup()
        if(mRecordFilterGroups.size > 0 && mRecordFilterGroups.last.filter == filterGroup){
            //没有做修改不需要记录操作
            return
        }
        val filter = GPUImageFilterGroup()
        filterGroup.filters.forEach {gpuimagefilter->
            val floatArray = mProgressQueue[gpuimagefilter.javaClass.simpleName]?.floatArray
            floatArray?.let {
                filter.addFilter(GPUImageFilterTools.createFilter(gpuimagefilter, it))
            }
        }
        val undoRedoModel = PetternUndoRedoModel(mPetternType)
        undoRedoModel.bitmap = bitmap
        undoRedoModel.filter = filter
        undoRedoModel.frameAssetsModel = frameAssetsModel
        drawBitmap?.let { p ->
            undoRedoModel.paths = p
        }

        mRecordFilterGroups.add(undoRedoModel)
        mPositionQueue.putAll(mCurrentPositions)

        val recordProgress = hashMapOf<String, FilterProgressState>()
        mProgressQueue.forEach {
            recordProgress[it.key] = it.value
        }
        mUndoProgressQueue.add(recordProgress)
        mCurrentPositions.clear()
        Log.e("mRecordFilterGroups", mRecordFilterGroups.toString())
    }

    /**
     * 还原一次操作
     */
    override fun resetLastFilterState(): PetternUndoRedoModel {
        val lastUndoRedoModel = PetternUndoRedoModel(PetternType.NORMAL)
        lastUndoRedoModel.apply {
            this.type = mRecordFilterGroups.last.type
            this.frameAssetsModel = mRecordFilterGroups.last.frameAssetsModel
            this.bitmap = mRecordFilterGroups.last.bitmap
            this.filter = mRecordFilterGroups.last.filter
            this.paths = mRecordFilterGroups.last.paths
        }
        mUndoRecordFilterGroups.add(lastUndoRedoModel)
        mRecordFilterGroups.removeLast()

        val lastProgressQueue: HashMap<String, FilterProgressState> = hashMapOf()
        lastProgressQueue.apply {
            mUndoProgressQueue.last.forEach {
                this[it.key] = it.value
            }
        }
        mRedoProgressQueue.add(lastProgressQueue)
        mUndoProgressQueue.removeLast()

        requestLastProgressQueue()

        Log.e("mProgressQueue", mProgressQueue.toString())

        val undoRedoModel = PetternUndoRedoModel(PetternType.NORMAL)
        if (!isResetFilter()) {
            undoRedoModel.type = PetternType.NORMAL
        }else{
            undoRedoModel.apply {
                this.type = mRecordFilterGroups.last.type
                this.frameAssetsModel = mRecordFilterGroups.last.frameAssetsModel
                this.bitmap = mRecordFilterGroups.last.bitmap
                this.filter = mRecordFilterGroups.last.filter
                this.paths = mRecordFilterGroups.last.paths
            }
        }
        return undoRedoModel
    }

    /**
     * 重置一次还原
     */
    fun redoResetLastFilterState(): PetternUndoRedoModel {
        val lastUndoRedoModel = PetternUndoRedoModel(PetternType.NORMAL)
        lastUndoRedoModel.apply {
            this.type = mUndoRecordFilterGroups.last.type
            this.frameAssetsModel = mUndoRecordFilterGroups.last.frameAssetsModel
            this.bitmap = mUndoRecordFilterGroups.last.bitmap
            this.filter = mUndoRecordFilterGroups.last.filter
            this.paths = mUndoRecordFilterGroups.last.paths
        }
        mRecordFilterGroups.add(lastUndoRedoModel)
        mUndoRecordFilterGroups.removeLast()

        val lastProgressQueue: HashMap<String, FilterProgressState> = hashMapOf()
        lastProgressQueue.apply {
            mRedoProgressQueue.last.forEach {
                this[it.key] = it.value
            }
        }
        mUndoProgressQueue.add(lastProgressQueue)
        mRedoProgressQueue.removeLast()

        requestLastProgressQueue()

        val undoRedoModel = PetternUndoRedoModel(PetternType.NORMAL)
        undoRedoModel.type = mRecordFilterGroups.last.type
        undoRedoModel.filter = mRecordFilterGroups.last.filter
        undoRedoModel.bitmap = mRecordFilterGroups.last.bitmap
        undoRedoModel.frameAssetsModel = mRecordFilterGroups.last.frameAssetsModel
        undoRedoModel.paths = mRecordFilterGroups.last.paths
        return undoRedoModel
    }

    fun clearRecordPositions(){
        mCurrentPositions.clear()
    }

    fun requestLastProgressQueue(){
        if(mUndoProgressQueue.size > 0){
            mProgressQueue.clear()
            mUndoProgressQueue.last.forEach {
                mProgressQueue[it.key] = it.value
            }
        }else{
            mProgressQueue.clear()
        }
        Log.e("mProgressQueue", mProgressQueue.toString())
    }

}