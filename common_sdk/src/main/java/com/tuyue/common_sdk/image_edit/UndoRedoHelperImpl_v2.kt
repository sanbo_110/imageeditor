package com.tuyue.common_sdk.image_edit

import android.graphics.Bitmap
import android.text.TextUtils
import android.util.Log
import com.tuyue.common_sdk.model.*
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import java.util.*

class UndoRedoHelperImpl_v2 : IFilterGroupHelper_v2 {

    //撤销队列
    private val mUndoList: LinkedList<PetternUndoRedoModel_v2> = LinkedList()
    //重置队列
    private val mRedoList: LinkedList<PetternUndoRedoModel_v2> = LinkedList()

    //二级撤销队列
    private val mSecondUndoList: LinkedList<PetternUndoRedoModel_v2> = LinkedList()
    //二级重置队列
    private val mSecondRedoList: LinkedList<PetternUndoRedoModel_v2> = LinkedList()

    //临时操作状态
    private var mCurrentUndoRedoModel = PetternUndoRedoModel_v2(PetternType.NORMAL)

    /**
     * 当前操作状态中添加一个滤镜，纹理
     * @param filterModel 所有[GPUImageFilter]相关参数
     * @param position 记录位置用于还原一级菜单位置
     */
    override fun addFilter(
        filterModel: GPUImageModel,
        petternType: PetternType,
        position: Int?
    ) {
        //滤镜和纹理中仅有一个效果，删除重复类型
        if (petternType == PetternType.FILTER || petternType == PetternType.TEXTURE) {
            try {
                //由于删除操作会越界，所以在try模块中运行
                for (i in 0..mCurrentUndoRedoModel.filter.lastIndex) {
                    if(mCurrentUndoRedoModel.filter[i].type == petternType){
                        mCurrentUndoRedoModel.filter.removeAt(i)
                    }
                }
            }catch (e: Exception){

            }
        }
        //调整中可以有多个滤镜叠加效果，去重复元素
        if(contains(mCurrentUndoRedoModel.filter, filterModel)) {
            for (i in 0..mCurrentUndoRedoModel.filter.lastIndex) {
                if (TextUtils.equals(
                        mCurrentUndoRedoModel.filter[i].filter?.javaClass?.simpleName,
                        filterModel.filter?.javaClass?.simpleName
                    )
                ) {
                    mCurrentUndoRedoModel.filter.add(i, filterModel)
                    mCurrentUndoRedoModel.filter.removeAt(i + 1)
                    break
                }
            }
        }else {
            mCurrentUndoRedoModel.filter.add(filterModel)
        }
        Log.e("addFilter", mCurrentUndoRedoModel.toString())
    }

    /**
     * 添加调整
     * 注意：调整中操作存在二级撤销，每次添加调整滤镜到当前状态后，直接保存二级操作状态，并且清空已经撤销的缓存
     */
    override fun addAdjust(filterModel: GPUImageModel, petternType: PetternType, position: Int?) {
        addFilter(filterModel, petternType, position)
        //当撤销队列更新状态时，清除重置缓存操作
        resetRedoState()
        recordSecondUndoRedoState()
    }

    /**
     * 重置
     */
    override fun redo(): PetternUndoRedoModel_v2 {
        if (!isCanRedo()) {
            return mCurrentUndoRedoModel
        }
        mUndoList.add(mRedoList.last.clone())
        mCurrentUndoRedoModel = mUndoList.last.clone()
        mRedoList.removeLast()
        return mCurrentUndoRedoModel
    }

    /**
     * 撤销
     */
    override fun undo(): PetternUndoRedoModel_v2 {
        if (!isCanUndo()) {
            return PetternUndoRedoModel_v2(PetternType.NORMAL)
        }
        mRedoList.add(mUndoList.last.clone())
        mUndoList.removeLast()

        mCurrentUndoRedoModel = if (isCanUndo()) {
            mUndoList.last.clone()
        } else {
            PetternUndoRedoModel_v2(PetternType.NORMAL)
        }
        return mCurrentUndoRedoModel
    }

    /**
     * 是否可以撤销
     */
    override fun isCanUndo(): Boolean {
        return mUndoList.size > 0
    }

    /**
     * 是否可以重置
     */
    override fun isCanRedo(): Boolean {
        return mRedoList.size > 0
    }

    /**
     * 确认操作，保存撤销队列
     */
    override fun recordUndoRedoState(
        petternType: PetternType,
        bitmap: Bitmap?,
        frameAssetsModel: FrameAssetsModel?,
        drawBitmap: Bitmap?
    ) {
        mCurrentUndoRedoModel.type = petternType
        mCurrentUndoRedoModel.bitmap = bitmap
        mCurrentUndoRedoModel.frameAssetsModel = frameAssetsModel
        mCurrentUndoRedoModel.paths = drawBitmap
        if(mSecondUndoList.size > 0){
            mUndoList.add(mSecondUndoList.last.clone())
            mCurrentUndoRedoModel = mUndoList.last.clone()
        }else{
            val lastUndoRedoModel = mCurrentUndoRedoModel.clone()
            mUndoList.add(lastUndoRedoModel)
        }
        clearSecondStack()
    }

    /**
     * 记录菜单初始化位置
     */
    override fun recordPositionByType(
        petternType: PetternType,
        position: Int,
        hasProgressBar: Boolean
    ) {
        val model = SelectRecordModel(position, hasProgressBar)
        mCurrentUndoRedoModel.positionMaps[petternType] = model
    }

    /**
     * 获取[PetternType]类型上次记录的位置
     */
    override fun getLastPositionByType(petternType: PetternType): SelectRecordModel? {
        return mCurrentUndoRedoModel.positionMaps[petternType]
    }

    /**
     * 获取滤镜最后一次保存的进度条参数
     */
    override fun getLastFilterProgress(filter: GPUImageFilter): FilterProgressState? {
        val filterModelList = mCurrentUndoRedoModel.filter
        var progressModel: FilterProgressState? = null
        filterModelList.forEach {
            if (TextUtils.equals(it.filter?.javaClass?.simpleName, filter.javaClass.simpleName)) {
                progressModel = it.progressState
            }
        }
        return progressModel
    }

    /**
     * 清除[PetternType]类型的滤镜
     */
    override fun clearFilterByType(petternType: PetternType) {
        val newlist = mutableListOf<GPUImageModel>()
        mCurrentUndoRedoModel.filter?.let { list ->
            list.forEach {
                if (it.type != petternType) {
                    newlist.add(it)
                }
            }
        }
        mCurrentUndoRedoModel.filter = newlist
    }

    /**
     * 清除所有状态
     */
    override fun clearAllState() {
        mCurrentUndoRedoModel = PetternUndoRedoModel_v2(PetternType.NORMAL)
    }

    override fun getFilterGroup(): GPUImageFilter {
        return mCurrentUndoRedoModel.getGPUImageFilterGroup()
    }

    /**
     * 是否可以二级撤销
     */
    override fun isCanSecondUndo(): Boolean {
        return mSecondUndoList.size > 0
    }

    /**
     * 是否可以二级重置
     */
    override fun isCanSecondRedo(): Boolean {
        return mSecondRedoList.size > 0
    }

    /**
     * 保存二级撤销状态
     */
    override fun recordSecondUndoRedoState(
        petternType: PetternType,
        bitmap: Bitmap?,
        frameAssetsModel: FrameAssetsModel?,
        drawBitmap: Bitmap?
    ) {
        mCurrentUndoRedoModel.type = petternType
        mCurrentUndoRedoModel.bitmap = bitmap
        mCurrentUndoRedoModel.frameAssetsModel = frameAssetsModel
        mCurrentUndoRedoModel.paths = drawBitmap
        val lastUndoRedoModel = mCurrentUndoRedoModel.clone()
        mSecondUndoList.add(lastUndoRedoModel)
    }

    /**
     * 二级重置
     */
    override fun redoSecond(): PetternUndoRedoModel_v2 {
        if (!isCanSecondRedo()) {
            return mCurrentUndoRedoModel
        }
        mSecondUndoList.add(mSecondRedoList.last.clone())
        mSecondRedoList.removeLast()
        mCurrentUndoRedoModel = mSecondUndoList.last.clone()
        return mCurrentUndoRedoModel
    }

    /**
     * 二级撤销
     */
    override fun undoSecond(): PetternUndoRedoModel_v2 {
        if (!isCanSecondUndo()) {
            return PetternUndoRedoModel_v2(PetternType.NORMAL)
        }
        mSecondRedoList.add(mSecondUndoList.last.clone())
        mSecondUndoList.removeLast()

        mCurrentUndoRedoModel = if (isCanSecondUndo()) {
            mSecondUndoList.last.clone()
        } else {
            //二级撤销状态不存在，返回一级撤销状态
            if(isCanUndo()){
                mUndoList.last.clone()
            }else{
                PetternUndoRedoModel_v2(PetternType.NORMAL)
            }
        }
        return mCurrentUndoRedoModel
    }

    override fun clearSecondStack() {
        mSecondUndoList.clear()
        mSecondRedoList.clear()
    }

    /**
     * 获取最后一次操作状态
     */
    override fun getLastUndoState(): PetternUndoRedoModel_v2? {
        if(mUndoList.size > 0){
            return mUndoList.last
        }
        return null
    }

    override fun getLastRedoState(): PetternUndoRedoModel_v2? {
        if(mRedoList.size > 0){
            return mRedoList.last
        }
        return null
    }

    override fun backLastState() {
        if(mUndoList.size > 0){
            mCurrentUndoRedoModel = mUndoList.last.clone()
        }
    }

    override fun resetRedoState() {
        mSecondRedoList.clear()
        mRedoList.clear()
    }

    fun getSelectFilter(filter: GPUImageFilter): GPUImageFilter? {
        val filterModelList = mCurrentUndoRedoModel.filter
        var selectFilter: GPUImageFilter? = null
        filterModelList.forEach {
            if (TextUtils.equals(it.filter?.javaClass?.simpleName, filter.javaClass.simpleName)) {
                selectFilter = it.filter
            }
        }
        return selectFilter
    }

    private fun contains(mutableList: MutableList<GPUImageModel>, filter: GPUImageModel): Boolean{
        for (i in 0..mutableList.lastIndex){
            if(TextUtils.equals(mutableList[i].filter?.javaClass?.simpleName, filter.filter?.javaClass?.simpleName)){
                return true
            }
        }
        return false
    }

    fun isUndoType(petternType: PetternType): Boolean{
        return isCanUndo() && mUndoList.last.type == petternType
    }

    fun isRedoType(petternType: PetternType): Boolean{
        return isCanRedo() && mRedoList.last.type == petternType
    }

}