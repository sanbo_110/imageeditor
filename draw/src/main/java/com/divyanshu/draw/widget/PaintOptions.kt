package com.divyanshu.draw.widget

import android.graphics.BlurMaskFilter
import android.graphics.Color
import android.graphics.MaskFilter

data class PaintOptions(var color: Int = Color.BLACK, var strokeWidth: Float = 25f,
                        var alpha: Int = 255, var isEraserOn: Boolean = false,
                        var maskFilter: MaskFilter? = BlurMaskFilter(10f, BlurMaskFilter.Blur.NORMAL)
)
