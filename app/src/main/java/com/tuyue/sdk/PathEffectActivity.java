package com.tuyue.sdk;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.SumPathEffect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class PathEffectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(new PathEffectView(this));
    }

    public class PathEffectView extends View {
        private float mPhase;// 偏移值
        private Paint mPaint;// 画笔对象
        private Path mPath;// 路径对象
        private PathEffect[] mEffects;// 路径效果数组

        public PathEffectView(Context context) {
            this(context, null);
        }

        public PathEffectView(Context context, AttributeSet attrs) {
            super(context, attrs);

            mPaint = new Paint();
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(10);
            mPaint.setColor(Color.DKGRAY);

            // 实例化路径
            mPath = new Path();

            // 定义路径的起点
            mPath.moveTo(0, 0);

            // 定义路径的各个点
            for (int i = 0; i <= 30; i++) {
                mPath.lineTo(i * 35, (float) (Math.random() * 100));
            }

            // 创建路径效果数组
            mEffects = new PathEffect[7];
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            mEffects[0] = null;
            //圆滑路径
            mEffects[1] = new CornerPathEffect(10);
            //离散路径
            mEffects[2] = new DiscretePathEffect(6.0F, 10.0F);
            //虚线
            mEffects[3] = new DashPathEffect(new float[]{20, 20, 20, 20}, mPhase);
            Path path = new Path();
            path.addRoundRect(0, 0, 20, 10, new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, Path.Direction.CCW);
            //圆点虚线
            mEffects[4] = new PathDashPathEffect(path, 30, mPhase, PathDashPathEffect.Style.ROTATE);
            mEffects[5] = new ComposePathEffect(mEffects[2], mEffects[4]);
            mEffects[6] = new SumPathEffect(mEffects[2], mEffects[4]);

            for (int i = 0; i < mEffects.length; i++) {
                mPaint.setPathEffect(mEffects[i]);
                canvas.drawPath(mPath, mPaint);

                // 每绘制一条将画布向下平移250个像素
                canvas.translate(0, 150);
            }

            // 刷新偏移值并重绘视图实现动画效果
            mPhase += 1;
            invalidate();
        }
    }

}
