package com.tuyue.sdk

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tencent.bugly.crashreport.CrashReport
import com.tuyue.common_sdk.activity.ImagePetternActivity
import com.tuyue.common_sdk.activity.VipActivity
import com.tuyue.common_sdk.helper.PictureSelectorHelper
import com.tuyue.common_sdk.helper.SpannableHelper
import com.tuyue.common_sdk.image_edit.ImageEditor
import com.tuyue.common_sdk.tools.BaseControlCenter
import com.tuyue.common_sdk.tools.DensityUtil
import com.tuyue.core.util.PermissionsUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        BaseControlCenter.init(this.application, false)

        CrashReport.initCrashReport(applicationContext, "048951fdf0", false)

        btn_sdk.setOnClickListener {

            PermissionsUtil.verifyStoragePermissions(this)
            PictureSelectorHelper.openGallery(this, object : PictureSelectorHelper.Callback{
                override fun onResult(result: String) {
                    ImageEditor.get(this@MainActivity)
                        .isImmersionBar(false)
                        .start(result, null)
                }
            })
        }

        btn_effect.setOnClickListener {
//            startActivity(Intent(this, PathEffectActivity::class.java))
            startActivity(Intent(this, VipActivity::class.java))
        }

        btn_path_effect.setOnClickListener {
            startActivity(Intent(this, PathActivity::class.java))
        }

    }

}